use LWP::UserAgent;
use HTTP::Request;
use HTTP::Response;
use JSON;

# Crear el objeto UserAgent
my $ua = LWP::UserAgent->new;

# URL del endpoint
my $url = 'http://localhost:8080/AppDTEWS/api/sendDTE';

# Definir el cuerpo del JSON
my $json_data = {
    emisor => {
        rutemisor => '76040308-3',
        rznsoc => 'ESTEBAN GABRIEL GUENUL ALMONACID SERVICIOS INFORMATICOS EMPRESA INDIV',
        giroemis => 'SERVICIOS DE INGENIERIA',
        acteco => '429000',
        dirorigen => 'quillay 467 villa presidente rio',
        cmnaorigen => 'TALCAHUANO',
        ciudadorigen => 'CONCEPCION',
        fchresol => '2016-04-25',
        nroresol => '0',
        cdgsiisucur => '1',
    },
    receptor => {
        rutrecep => '77813960-K',
        rznsocrecep => 'AMULEN CONSULTORES LTDA',
        girorecep => 'ASESORIA TRIBUTARIA',
        dirrecep => 'URMENETA 305 OFIC 512',
        cmnarecep => 'puerto montt',
        ciudadrecep => 'puerto montt',
        rutcaratula => '60803000-K',
    },
    iddoc => {
        tipodte => '33',
        folio => '3',
        fchemis => '2024-01-29',
        fmapago => 1,
    },
    totales => {
        mntneto => 500000,
        iva => 95000,
        tasaiva => 19,
        mnttotal => 595000,
    },
    detalle => [
        {
            nrolindet => '1',
            cdgitem => [
                { tpocodigo => 'INT', vlrcodigo => '23' }
            ],
            unmditem => 'UN',
            nmbitem => 'SERVIDOR HP',
            qtyitem => 1,
            prcitem => 500000,
            descuentopct => 0,
            descuentomonto => 0,
            indexe => 0,
            montoitem => 500000,
        }
    ],
    usuario => {
        login => 'eguenul',
        rut => '13968481-8',
        password => 'amulen1956',
    }
};

# Convertir el hash a JSON
my $json_request = encode_json($json_data);

# Crear la solicitud HTTP
my $request = HTTP::Request->new(POST => $url);
$request->header('Content-Type' => 'application/json');
$request->content($json_request);

# Enviar la solicitud y recibir la respuesta
my $response = $ua->request($request);

# Comprobar la respuesta
if ($response->is_success) {
    print "Response: " . $response->decoded_content . "\n";
} else {
    print "HTTP Error: " . $response->status_line . "\n";
}
