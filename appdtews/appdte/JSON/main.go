package main

import (
    "encoding/json"
    "fmt"
    "io/ioutil"
    "net/http"
    "bytes"
)

// Definir las estructuras de los datos
type Emisor struct {
    RUTEmisor   string `json:"rutemisor"`
    RZNSoc      string `json:"rznsoc"`
    GiroEmis    string `json:"giroemis"`
    ActEco      string `json:"acteco"`
    DirOrigen   string `json:"dirorigen"`
    CmnaOrigen  string `json:"cmnaorigen"`
    CiudadOrigen string `json:"ciudadorigen"`
    FchResol    string `json:"fchresol"`
    NroResol    string `json:"nroresol"`
    CDGSII    string `json:"cdgsiisucur"`
}

type Receptor struct {
    RUTRecep    string `json:"rutrecep"`
    RZNSocRecep string `json:"rznsocrecep"`
    GiroRecep   string `json:"girorecep"`
    DirRecep    string `json:"dirrecep"`
    CmnaRecep   string `json:"cmnarecep"`
    CiudadRecep string `json:"ciudadrecep"`
    RUTCaratula string `json:"rutcaratula"`
}

type IDDoc struct {
    TipoDTE     string `json:"tipodte"`
    Folio       string `json:"folio"`
    FchEmis     string `json:"fchemis"`
    IndServicio string `json:"indservicio"`
    IndMntNeto  string `json:"indmntneto"`
}

type Totales struct {
    MntNeto   string `json:"mntneto"`
    IVA       string `json:"iva"`
    TasaIVA   string `json:"tasaiva"`
    MntTotal  string `json:"mnttotal"`
}

type CodigoItem struct {
    TipoCodigo string `json:"tpocodigo"`
    ValorCodigo string `json:"vlrcodigo"`
}

type Detalle struct {
    NroLinDet       string       `json:"nrolindet"`
    CodigoItem      []CodigoItem `json:"cdgitem"`
    UnidadItem      string       `json:"unmditem"`
    NombreItem      string       `json:"nmbitem"`
    CantidadItem    int          `json:"qtyitem"`
    PrecioItem      float64      `json:"prcitem"`
    DescuentoPct    float64      `json:"descuentopct"`
    DescuentoMonto  float64      `json:"descuentomonto"`
    Indexe          string       `json:"indexe"`
    MontoItem       float64      `json:"montoitem"`
}

type Usuario struct {
    Login    string `json:"login"`
    RUT      string `json:"rut"`
    Password string `json:"password"`
}

type RequestData struct {
    Emisor    Emisor    `json:"emisor"`
    Receptor  Receptor  `json:"receptor"`
    IDDoc     IDDoc     `json:"iddoc"`
    Totales   Totales   `json:"totales"`
    Detalle   []Detalle `json:"detalle"`
    Usuario   Usuario   `json:"usuario"`
}

func main() {
    // Crear los datos JSON
    requestData := RequestData{
        Emisor: Emisor{
            RUTEmisor:   "77940465-K",
            RZNSoc:      "CLAVATA SPA",
            GiroEmis:    "VENTA AL POR MENOR EN COMERCIOS ESPECIAL",
            ActEco:      "107100",
            DirOrigen:   "RAFAEL CASANOVA 297",
            CmnaOrigen:  "SANTA CRUZ",
            CiudadOrigen: "SANTA CRUZ",
            FchResol:    "2016-04-25",
            NroResol:    "0",
            CDGSII:    "1",
        },
        Receptor: Receptor{
            RUTRecep:    "9375855-2",
            RZNSocRecep: "LUZMIRA CESPEDES NAVARRO",
            GiroRecep:   "PROVISIONES",
            DirRecep:    "ADRIANO DIAZ 560",
            CmnaRecep:   "Santa Cruz",
            CiudadRecep: "Santa Cruz",
            RUTCaratula: "60803000-K",
        },
        IDDoc: IDDoc{
            TipoDTE:     "39",
            Folio:       "5",
            FchEmis:     "2024-07-10",
            IndServicio: "3",
            IndMntNeto:  "2",
        },
        Totales: Totales{
            MntNeto:   "15126",
            IVA:       "2874",
            TasaIVA:   "19",
            MntTotal:  "18000",
        },
        Detalle: []Detalle{
            {
                NroLinDet: "1",
                CodigoItem: []CodigoItem{
                    {
                        TipoCodigo: "INT",
                        ValorCodigo: "01001",
                    },
                },
                UnidadItem:     "UN",
                NombreItem:     "PAN CORRIENTE",
                CantidadItem:   10,
                PrecioItem:     1800,
                DescuentoPct:   0,
                DescuentoMonto: 0,
                Indexe:         "0",
                MontoItem:      18000,
            },
        },
        Usuario: Usuario{
            Login:    "eguenul",
            RUT:      "13968481-8",
            Password: "amulen1956",
        },
    }

    // Convertir los datos a JSON
    jsonData, err := json.Marshal(requestData)
    if err != nil {
        fmt.Println("Error al convertir a JSON:", err)
        return
    }

    // Enviar el JSON a un servidor
	url := "http://localhost:8080/api/sendDTE" // Reemplaza con la URL de tu servidor
    resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonData))
    if err != nil {
        fmt.Println("Error al enviar la solicitud:", err)
        return
    }
    defer resp.Body.Close()

    // Leer la respuesta del servidor
    responseBody, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        fmt.Println("Error al leer la respuesta:", err)
        return
    }

    // Imprimir la respuesta
    fmt.Println("Respuesta del servidor:", string(responseBody))
}
