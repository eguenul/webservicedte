package main

import (
    "bytes"
    "encoding/json"
    "fmt"
    "net/http"
)

// Definir las estructuras
type UsuarioJson struct {
    Login    string `json:"login"`
    RUT      string `json:"rut"`
    Password string `json:"password"`
}

type RequestData struct {
    RutEmisor string     `json:"rutemisor"`
    DvEmisor  string     `json:"dvemisor"`
    TrackID   string     `json:"trackid"`
    Usuario   UsuarioJson `json:"usuario"`
}

func main() {
    // Crear los datos JSON
    requestData := RequestData{
        RutEmisor: "77940465",
        DvEmisor:  "K",
        TrackID:   "22762668",
        Usuario: UsuarioJson{
            Login:    "eguenul",
            RUT:      "13968481-8",
            Password: "amulen1956",
        },
    }

    // Convertir los datos a JSON
    jsonData, err := json.Marshal(requestData)
    if err != nil {
        fmt.Println("Error al convertir a JSON:", err)
        return
    }

    // Definir la URL del servicio POST
	url := "http://localhost:8080/api/TrackingBoleta" // Reemplaza con la URL de tu servicio

    // Crear la solicitud POST
    resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonData))
    if err != nil {
        fmt.Println("Error al enviar solicitud POST:", err)
        return
    }
    defer resp.Body.Close()

    // Leer y mostrar la respuesta
    var responseBody []byte
    resp.Body.Read(responseBody)
    fmt.Println("Respuesta del servidor:", string(responseBody))
}
