#!/bin/bash

# Definir la URL del servidor
URL="http://localhost:8080/AppDTEWS/api/sendDTE"

# Crear el cuerpo de la solicitud en formato JSON
JSON_PAYLOAD=$(cat <<EOF
{
  "usuario": {
    "login": "eguenul",
    "rut": "13968481-8",
    "password": "amulen1956"
  },
  "iddoc": {
    "tipodte": "33",
    "folio": "1",
    "fchemis": "2024-05-09",
    "fmapago": "1"
  },
  "emisor": {
    "rutemisor": "76040308-3",
    "rznsoc": "egga informatica e.i.r.l",
    "giroemis": "servicios informaticos",
   "acteco": "429000",
    "dirorigen": "quillay 467 villa presidente rio",
    "cmnaorigen": "talcahuano",
    "ciudadorigen": "concepcion",
    "fchresol": "2016-04-25",
    "nroresol": "0",
    "cdgsiisucur": "1"
  },
  "receptor": {
    "rutrecep": "77813960-K",
    "rznsocrecep": "amulen consultores ltda",
    "girorecep": "giro receptor",
    "dirrecep": "urmeneta 305 oficina 513",
    "cmnarecep": "puerto montt",
    "ciudadrecep": "puerto montt",
	"rutcaratula":"60803000-K"
  },
  "totales": {
    "mntneto": "460782",
    "tasaiva": "19",
    "iva": "87549",
    "mnttotal": "548331"
  },
  "detalle": [
    {
      "nrolindet": "1",
      "cdgitem": [{"tpocodigo": "int", "vlrcodigo": "20"}],
      "nmbitem": "cajon afecto",
      "qtyitem": "141",
      "unmditem": "un",
      "prcitem": "1922",
      "montoitem": "271002",
	  "indexe":"0",
	  "descuentopct":"0",
	  "descuentomonto":"0"
      
    },
    {
      "nrolindet": "2",
      "cdgitem": [{"tpocodigo": "int", "vlrcodigo": "21"}],
      "nmbitem": "relleno afecto",
     
      "qtyitem": "60",
      "unmditem": "un",
      "prcitem": "3163",
      "montoitem": "189780",
	  "descuentopct":"0",
	  "descuentomonto":"0",
       "indexe":"0"	
	}
  ],
  "referencia": {
    "nrolinref": "1",
    "tpodocref": "801",
    "folioref": "5",
    "fchref": "2024-02-15",
    "razonref": "caso 3832272-1"
  }
}
EOF
)

# Hacer la solicitud POST utilizando curl
curl -X POST "$URL" \
  -H "Content-Type: application/json" \
  -d "$JSON_PAYLOAD"
