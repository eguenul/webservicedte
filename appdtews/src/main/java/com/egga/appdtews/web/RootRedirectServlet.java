/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.web;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RootRedirectServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
       String path = req.getRequestURI();
   System.out.print(path);         
// Si la solicitud es para un recurso estático, no redirigir
        if (path.startsWith("/css/") || path.startsWith("/scripts/") || path.startsWith("/images/")|| path.startsWith("/messageview/")) {
            return;
        }

        // Redirige todas las demás solicitudes
        resp.sendRedirect("/AppDTEWS/index");
    }
        
}
