/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/

package com.egga.appdtews.web;
import com.egga.appdtews.empresa.Empresa;
import com.egga.appdtews.include.ThymeleafConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 *
 * @author esteban
 */
public class IndexServlet extends HttpServlet {
    
    private TemplateEngine templateEngine;

    
    
     @Override
    public void init() {
        // Inicialización del TemplateEngine usando ThymeleafConfig
        ThymeleafConfig thymeleafConfig = new ThymeleafConfig();
        templateEngine = thymeleafConfig.createTemplateEngine();
    }
    
    
    
       
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        
        if (request.getSession().getAttribute("loginauth") == null) {
            response.sendRedirect("login"); // Si no está autenticado, redirigir a login
            return;
        }

        // Verificar si la empresa está seleccionada
        if (request.getSession().getAttribute("empresaid") == null) {
            response.sendRedirect("secured/selempresa"); // Si no hay empresa seleccionada, redirigir
            return;
        }
        // Obtener la empresa de la sesión
        Empresa objEmpresa = (Empresa) request.getSession().getAttribute("Empresa");

        // Verificar que la empresa esté correctamente cargada
        if (objEmpresa == null || objEmpresa.getEmpresaid() == 0) {
            response.sendRedirect("secured/selempresa"); // Redirigir a selección si es nula o incorrecta
            return;
        }
        
        
        // Todo está correcto, redirigir al JSP de índice
        
          Context context = new Context();
          context.setVariable("empresa",objEmpresa );
           
                
         
          String contenido = templateEngine.process("index", context);
          response.setContentType("text/html");
          response.getWriter().write(contenido);
   
        
    }

        
    }
            
    
    

 

