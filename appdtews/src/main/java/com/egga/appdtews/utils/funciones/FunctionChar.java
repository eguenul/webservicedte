/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.utils.funciones;

/**
 *
 * @author esteban
 */
public class FunctionChar {
    

public String formatoTexto(String parmcadena){
    
  String cadena = parmcadena;
  
  cadena = cadena.replaceAll("Ñ","N");
  cadena = cadena.replaceAll("ñ","n");
  
  /* LETRAS MINUSCULAS */
  cadena = cadena.replaceAll("á","a");
  cadena = cadena.replaceAll("é","e");
  cadena = cadena.replaceAll("í","i");
  cadena = cadena.replaceAll("ó","u");
  cadena = cadena.replaceAll("ú","u");
  
  /* LETRAS MAYUSCULAS */
  cadena = cadena.replaceAll("Á","A");
  cadena = cadena.replaceAll("É","E");
  cadena = cadena.replaceAll("Í","I");
  cadena = cadena.replaceAll("Ó","O");
  cadena = cadena.replaceAll("Ú","U"); 
  cadena = cadena.replaceAll("&","Y");
  

  return cadena;    
    
}




}
