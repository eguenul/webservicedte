/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.include;
import java.io.IOException;
import java.sql.*;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
public class Conexion {
   private  Connection cnx = null;
   public void Conectar() throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException {
      if (this.cnx == null) {
         try {
            System.out.print("conectando");
          ConfigAppDTEWS objconfig = new ConfigAppDTEWS();
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.cnx = DriverManager.getConnection("jdbc:mysql://"+objconfig.getServeraddress()+":3306/"+objconfig.getDatabasename()+"?allowPublicKeyRetrieval=true&useSSL=false", objconfig.getUsername(),objconfig.getUserpass());
            
            Statement stmt2 = cnx.createStatement();
             stmt2.execute("SET CHARACTER SET utf8");
            
         } catch (SQLException ex) {
            throw new SQLException(ex);
         } catch (ClassNotFoundException ex) {
            throw new ClassCastException(ex.getMessage());
         }
      }
     
   }
   public  void cerrar() throws SQLException {
     
         this.cnx.close();
      
   }
   
   public  Connection getConexion() throws SQLException {
     
        return this.cnx;
      
   }

    public Connection obtener() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
   
   
}

