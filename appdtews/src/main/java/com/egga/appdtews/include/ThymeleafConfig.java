
package com.egga.appdtews.include;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

public class ThymeleafConfig {

    public TemplateEngine createTemplateEngine() {
        // Configurar Thymeleaf TemplateResolver
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("/templates/");  // Carpeta de plantillas
        templateResolver.setSuffix(".html");        // Extensión de archivo
        templateResolver.setTemplateMode("HTML");

        // Crear TemplateEngine
        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        return templateEngine;
    }
}