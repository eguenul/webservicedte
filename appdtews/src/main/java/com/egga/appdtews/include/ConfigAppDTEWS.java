/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.include;



import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class ConfigAppDTEWS {
    private final String serveraddress;
    private final String username;
    private final String userpass;
    private final String databasename;
    private final String pathreports;
    private final String pathdownload;
    
    public ConfigAppDTEWS() throws ParserConfigurationException, SAXException, IOException{
      /*
        Properties prop = new Properties();
       try (InputStream input = getClass().getClassLoader().getResourceAsStream("appdtews.properties")) {
    if (input == null) {
        throw new FileNotFoundException("No se encontró el archivo appdtews.properties en el classpath.");
    }
    prop.load(input);
*/
       String configFilePath =  "appdtews.properties";

Properties prop = new Properties();
try (InputStream input = new FileInputStream(configFilePath)) {
           
           if (input == null) {
        throw new FileNotFoundException("No se encontró el archivo appdtews.properties en el classpath.");
    }
    prop.load(input);

          this.serveraddress = prop.getProperty("server-address");
          this.username = prop.getProperty("user-name");
          this.userpass = prop.getProperty("user-pass");
          this.databasename = prop.getProperty("database-name");
          this.pathreports = prop.getProperty("path-reports");
          this.pathdownload = prop.getProperty("path-download");
        }
    }
    
    
    
    public String getUserpass() {
        return userpass;
    }

    public String getServeraddress() {
        return serveraddress;
    }

    public String getUsername() {
        return username;
    }
    
    
  

    public String getDatabasename() {
        return databasename;
    }

    public String getPathreports() {
        return pathreports;
    }

    /**
     * @return the pathdownload
     */
    public String getPathdownload() {
        return pathdownload;
    }
    
    

    
}