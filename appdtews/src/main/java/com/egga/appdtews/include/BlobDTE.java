/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.include;

import com.egga.appdtews.appdte.sii.utilidades.ConfigAppDTE;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class BlobDTE {
    
    
    
    public  void getXMLDTE(int idmovimiento) throws ParserConfigurationException, SAXException, IOException, SQLException, ClassNotFoundException {
         System.setProperty("file.encoding", "ISO-8859-1");
     
            ConfigAppDTE objconfig = new ConfigAppDTE();
        Conexion objaux = new Conexion();
      objaux.Conectar();
        
        Statement stmt = null;
        InputStream input = null;
        FileOutputStream output = null;
        try {
            String sql = "SELECT * from Movimiento where MovimientoId="+ String.valueOf(idmovimiento);
           System.out.print(sql);
            stmt = objaux.getConexion().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
   
            if (rs.next()) {
                File file = new File(objconfig.getPathdte()+rs.getString("ArchivoNom"));
                 output = new FileOutputStream(file);

                System.out.println("Leyendo archivo desde la base de datos...");
                byte[] buffer = rs.getBytes("BlobDTE");
                
                    output.write(buffer);
                
                System.out.println("> Archivo guardado en : " + file.getAbsolutePath());
                objaux.cerrar();
            }
        } catch (SQLException | IOException ex) {
            System.err.println(ex.getMessage());
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
                if (output != null) {
                    output.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
            } catch (IOException | SQLException ex) {
                System.err.println(ex.getMessage());
            }
        }

}
    
    
    
}
