/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.service;

import com.egga.appdtews.appboleta.sii.trackBOLETA;
import com.egga.appdtews.appdte.json.TrackingJSON;
import com.egga.appdtews.appdte.json.UsuarioJson;
import com.egga.appdtews.include.Funciones;
import com.egga.appdtews.login.LoginModel;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class TrackingBoletaServiceImpl implements TrackingBoletaService {

    @Override
    public String trackBoleta(TrackingJSON objTrack) throws ParserConfigurationException, SAXException, IOException, Exception {
        UsuarioJson objUsuario = objTrack.getUsuario();

        LoginModel objLoginModel = new LoginModel();
        boolean flagLogin = objLoginModel.authLogin(objUsuario.getLogin(), objUsuario.getPassword());

        if (flagLogin) {
            Funciones objFunciones = new Funciones();
            objFunciones.loadCert(objUsuario.getLogin());
        }

        String claveFirma = objLoginModel.getClaveFirma(objUsuario.getLogin(), objUsuario.getPassword());
        trackBOLETA objTrackBOLETA = new trackBOLETA();

        return objTrackBOLETA.getESTATE(
            objUsuario.getLogin(),
            claveFirma,
            objTrack.getRutemisor(),
            objTrack.getDvemisor(),
            objTrack.getTrackid()
        );
    }
}