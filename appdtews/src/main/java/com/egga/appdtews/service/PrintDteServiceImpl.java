/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.service;


import com.egga.appdtews.appdte.json.PrintJSON;
import com.egga.appdtews.documento.DocumentoModel;
import com.egga.appdtews.empresa.EmpresaModel;
import com.egga.appdtews.include.BlobDTE;
import com.egga.appdtews.movimientos.MovimientoModel;
import com.egga.appdtews.appdte.sii.utilidades.ConfigAppDTE;
import com.egga.appdtews.appdte.sii.utilidades.PrintBOLETA;
import com.egga.appdtews.appdte.sii.utilidades.PrintDTE;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class PrintDteServiceImpl implements PrintDteService {

    @Override
    public String generatePdf(PrintJSON requestjson) throws ParserConfigurationException, SAXException, IOException {
        try {
            String rut = requestjson.getRut();
            String folio = requestjson.getFolio();
            String codsii = requestjson.getCodsii();

            DocumentoModel objDocumentoModel = new DocumentoModel();
            int idtipodoc = objDocumentoModel.getId(codsii);

            MovimientoModel objMovimientoModel = new MovimientoModel();
            EmpresaModel objEmpresaModel = new EmpresaModel();
            int empresaid = objEmpresaModel.getIdEmpresafromRut(rut);

            int idmovimiento = objMovimientoModel.getIdMovimiento(idtipodoc, Integer.parseInt(folio), empresaid);

            BlobDTE objBlob = new BlobDTE();
            objBlob.getXMLDTE(idmovimiento);

            ConfigAppDTE objConfig = new ConfigAppDTE();
            String[] arrayrutemisor = rut.split("-");

            if ("39".equals(codsii) || "41".equals(codsii)) {
                PrintBOLETA objPrintBOLETA = new PrintBOLETA();
                objPrintBOLETA.printBOLETA(arrayrutemisor[0] + "F" + folio + "T" + codsii);
            } else {
                PrintDTE objPrintDTE = new PrintDTE();
                objPrintDTE.printDTE(arrayrutemisor[0] + "F" + folio + "T" + codsii);
            }

            String nombredte = objConfig.getPathpdf() + "ENVDTE" + arrayrutemisor[0] + "F" + folio + "T" + codsii + ".pdf";
            byte[] bytesContent = Files.readAllBytes(Paths.get(nombredte));
            String auxbas64 =  Base64.getEncoder().encodeToString(bytesContent);
           System.out.print(auxbas64);
            return Base64.getEncoder().encodeToString(bytesContent);

        } catch (Exception e) {
            throw new IOException("Error while generating PDF: " + e.getMessage(), e);
        }
    }
}