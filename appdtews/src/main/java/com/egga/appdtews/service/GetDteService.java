/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.service;

import com.egga.appdtews.appdte.json.PrintJSON;
import java.io.IOException;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public interface GetDteService {
    /**
     * Obtiene el XML correspondiente a un DTE en formato codificado Base64.
     * 
     * @param requestjson Objeto que contiene los datos necesarios para buscar el DTE.
     * @return XML codificado en Base64.
     * @throws SAXException, IOException, ClassNotFoundException, SQLException, ParserConfigurationException
     */
    String getXmlBase64(PrintJSON requestjson) throws SAXException, IOException, ClassNotFoundException, SQLException, ParserConfigurationException;
}