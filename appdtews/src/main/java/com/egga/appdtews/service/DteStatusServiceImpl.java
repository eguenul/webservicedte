/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.service;


import com.egga.appdtews.appboleta.sii.ConsultaBOLETA;
import com.egga.appdtews.appdte.json.ConsultaDTEjson;
import com.egga.appdtews.appdte.json.DatosDTE;
import com.egga.appdtews.appdte.json.UsuarioJson;
import com.egga.appdtews.appdte.sii.funcionesws.ConsultaDTE;
import com.egga.appdtews.appdte.sii.utilidades.ConfigAppDTE;
import com.egga.appdtews.login.LoginModel;
import java.io.IOException;
import java.sql.SQLException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.xml.sax.SAXException;

public class DteStatusServiceImpl implements DteStatusService {
    
    @Override
    public String getState(ConsultaDTEjson consulta) throws Exception {
        DatosDTE datosDTE = consulta.getDatosDTE();

        // Validar tipo de documento
        if ("39".equals(datosDTE.getTipodte()) || "41".equals(datosDTE.getTipodte())) {
            return processBoleta(datosDTE, consulta.getUsuario());
        } else {
            return processGenericDte(datosDTE, consulta.getUsuario());
        }
    }

    private String processBoleta(DatosDTE datosDTE, UsuarioJson usuario) throws ParseException, SQLException, SAXException, IOException, Exception {
        ConsultaBOLETA consultaBOLETA = new ConsultaBOLETA();
        consultaBOLETA.setRutCompania(datosDTE.getRutcompania());
        consultaBOLETA.setDvCompania(datosDTE.getDvcompania());
        consultaBOLETA.setRutConsultante(datosDTE.getRutconsultante());
        consultaBOLETA.setDvConsultante(datosDTE.getDvconsultante());
        consultaBOLETA.setFolioDte(datosDTE.getFoliodte());
        consultaBOLETA.setTipoDte(datosDTE.getTipodte());
        consultaBOLETA.setMontoDte(datosDTE.getMontodte());
        consultaBOLETA.setRutReceptor(datosDTE.getRutreceptor());
        consultaBOLETA.setDvReceptor(datosDTE.getDvreceptor());
        consultaBOLETA.setFechaEmisionDte(formatDate(datosDTE.getFechaemisiondte(), "yyyy-MM-dd", "dd-MM-yyyy"));

        // Obtener clave firma
        LoginModel loginModel = new LoginModel();
        String claveFirma = loginModel.getClaveFirma(usuario.getLogin(), usuario.getPassword());

        return consultaBOLETA.getEstDte(usuario.getLogin(), claveFirma);
    }

    private String processGenericDte(DatosDTE datosDTE, UsuarioJson usuario) throws Exception {
        ConfigAppDTE config = new ConfigAppDTE();
        ConsultaDTE consultaDTE = new ConsultaDTE();
        consultaDTE.setRutCompania(datosDTE.getRutcompania());
        consultaDTE.setDvCompania(datosDTE.getDvcompania());
        consultaDTE.setRutConsultante(datosDTE.getRutconsultante());
        consultaDTE.setDvConsultante(datosDTE.getDvconsultante());
        consultaDTE.setFolioDte(datosDTE.getFoliodte());
        consultaDTE.setTipoDte(datosDTE.getTipodte());
        consultaDTE.setMontoDte(datosDTE.getMontodte());
        consultaDTE.setRutReceptor(datosDTE.getRutreceptor());
        consultaDTE.setDvReceptor(datosDTE.getDvreceptor());
        consultaDTE.setFechaEmisionDte(formatDate(datosDTE.getFechaemisiondte(), "yyyy-MM-dd", "ddMMyyyy"));

        LoginModel loginModel = new LoginModel();

        // Verificar autenticación
        if (loginModel.authLogin(usuario.getLogin(), usuario.getPassword())) {
            String claveFirma = loginModel.getClaveFirma(usuario.getLogin(), usuario.getPassword());
            return consultaDTE.getEstDte(usuario.getLogin(), claveFirma, config.getPathenvironment());
        }

        throw new Exception("Usuario no autenticado");
    }

    private String formatDate(String date, String inputFormat, String outputFormat) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(inputFormat);
        Date parsedDate = formatter.parse(date);
        formatter.applyPattern(outputFormat);
        return formatter.format(parsedDate);
    }
}