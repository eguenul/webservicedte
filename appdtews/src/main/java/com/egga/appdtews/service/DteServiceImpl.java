/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.service;
import com.egga.appdtews.documento.DocumentoModel;
import com.egga.appdtews.empresa.EmpresaModel;
import com.egga.appdtews.include.Funciones;
import com.egga.appdtews.login.LoginModel;
import com.egga.appdtews.appboleta.sii.sendBOLETA;
import com.egga.appdtews.appdte.json.*;
import com.egga.appdtews.appdte.sii.utilidades.AppDTE;
import com.egga.appdtews.appdte.sii.utilidades.ConfigAppDTE;
import com.egga.appdtews.appdte.sii.utilidades.FuncionesCAF;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.StringWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DteServiceImpl implements DteService  {
    private static final Logger logger = LoggerFactory.getLogger(DteServiceImpl.class);
    @Override
    public String processDte(DteJson objDTE) throws Exception {
        try{
        logger.info("Iniciando procesamiento de DTE para el usuario: {}");

     
        ObjectMapper objectMapper = new ObjectMapper();
        StringWriter stringWriter = new StringWriter();
	objectMapper.writeValue(stringWriter, objDTE);
        final String stringDTE = stringWriter.toString();
        
        
      
        
        UsuarioJson objUsuario = objDTE.getUsuario();
        EmisorJson objemisor = objDTE.getEmisor();
        IdDteJson iddoc = objDTE.getIddoc();

   
        // Autenticación
        LoginModel objLoginModel = new LoginModel();
        boolean flag_senddte = objLoginModel.authLogin(objUsuario.getLogin(), objUsuario.getPassword());

        if (!flag_senddte) {
            return createErrorResponse(objemisor, objUsuario);
        }

        // Carga de certificados y CAF
        Funciones objFunciones = new Funciones();
        objFunciones.loadCert(objUsuario.getLogin());
        String clavefirma = objLoginModel.getClaveFirma(objUsuario.getLogin(), objUsuario.getPassword());

        EmpresaModel objEmpresaModel = new EmpresaModel();
        int empresaid = objEmpresaModel.getIdEmpresafromRut(objemisor.getRutemisor());
        DocumentoModel objDocumentoModel = new DocumentoModel();
        int tipodocid = objDocumentoModel.getId(iddoc.getTipodte());
        objFunciones.loadCAF(empresaid, tipodocid, objUsuario.getLogin());

        FuncionesCAF objFuncionCAF = new FuncionesCAF();
        ConfigAppDTE objConfig = new ConfigAppDTE();
        flag_senddte = objFuncionCAF.validaCAF(
            objUsuario.getLogin(),
            objConfig.getPathcaf(),
            objemisor.getRutemisor(),
            Integer.parseInt(iddoc.getTipodte()),
            Integer.parseInt(iddoc.getFolio())
        );

        if (!flag_senddte) {
            return createErrorResponse(objemisor, objUsuario);
        }

        // Enviar DTE
        if ("39".equals(iddoc.getTipodte()) || "41".equals(iddoc.getTipodte())) {
            sendBOLETA objBOLETA = new sendBOLETA();
            String stringBOLETA = objBOLETA.sendBOLETA(stringDTE, objUsuario.getLogin(), clavefirma, objUsuario.getRut());

            return parseBoletaResponse(stringBOLETA);
        } else {
            AppDTE objAppDTE = new AppDTE();
            return objAppDTE.sendDTE(stringDTE, objUsuario.getLogin(), clavefirma, objUsuario.getRut(), true);
        }
    }catch(Exception e){
        
         logger.error("Error procesando DTE: {}", e.getMessage(), e);  // Log de error con el mensaje y la excepción
            throw e; // Lanzamos la excepción nuevamente si es necesario para ser manejada en otros lugares
    
    }
    }
    
    
    private String createErrorResponse(EmisorJson objEmisor, UsuarioJson objUsuario) throws IOException {
        TrackID objTrackID = new TrackID();
        objTrackID.setRut_emisor(objEmisor.getRutemisor());
        objTrackID.setRut_envia(objUsuario.getRut());
        objTrackID.setFecha_recepcion("-");
        objTrackID.setTrackid("0");
        objTrackID.setEstado("ERROR");
        ObjectMapper objectMapper = new ObjectMapper();
	StringWriter stringTRACK = new StringWriter();
	objectMapper.writeValue(stringTRACK, objTrackID);	       
        return stringTRACK.toString();
    }

    private String parseBoletaResponse(String stringBOLETA) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
	//convert json string to object
	TrackID	objTrackID = objectMapper.readValue(stringBOLETA, TrackID.class);
       StringWriter stringTRACK = new StringWriter();
        objectMapper.writeValue(stringTRACK, objTrackID);	       
        return stringTRACK.toString();
    }
}