/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.login;

import com.egga.appdtews.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class LoginModel {
    
    public LoginModel() {
          
    }
    
   public boolean authLogin(String loginname,String password) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            
            boolean authlogin = false;
            String sql;
            Statement stmt = objConexion.getConexion().createStatement();
            sql = "Select * from Usuario where UsuarioLogin='"+loginname+"' and UsuarioPass='"+password+"'";
            
            ResultSet objrecordset = stmt.executeQuery(sql);
            
            if(objrecordset.next()==true){
                authlogin=true;
            }
            objConexion.cerrar();
            return authlogin;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(LoginModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
   }
   
   public String getRut(String loginname) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            String sql;
            Statement stmt = objConexion.getConexion().createStatement();
            sql = "Select * from Usuario where UsuarioLogin='"+loginname+"'";
            String rutusuario  = "";
            ResultSet objrecordset = stmt.executeQuery(sql);
            
            if(objrecordset.next()==true){
                rutusuario = objrecordset.getString("UsuarioRut");
            }
            objConexion.cerrar();
            return rutusuario;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(LoginModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
   
   }
   
  
   public String getClaveFirma(String loginname,String password) throws SQLException{
        try {
            Conexion objConexion = new Conexion();
            objConexion.Conectar();
            
            
            
            String ClaveFirma = null;
            String sql;
            Statement stmt = objConexion.getConexion().createStatement();
            sql = "Select ClaveFirma from Usuario where UsuarioLogin='"+loginname+"' and UsuarioPass='"+password+"'";
            
            ResultSet objrecordset = stmt.executeQuery(sql);
            
            if(objrecordset.next()==true){
            ClaveFirma = objrecordset.getString("ClaveFirma");
            }
            objConexion.cerrar();
            return ClaveFirma;
        } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(LoginModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
   }
   
   
   
   
    
}
