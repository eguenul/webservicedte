/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.login;

import com.egga.appdtews.include.ThymeleafConfig;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xml.sax.SAXException;


/**
 *
 * @author esteban
 */

@WebServlet(urlPatterns = "/setpass", name = "setpass")
public class SetPassServlet extends HttpServlet {
    
 private TemplateEngine templateEngine;
     @Override
    public void init() {
        // Inicialización del TemplateEngine usando ThymeleafConfig
        ThymeleafConfig thymeleafConfig = new ThymeleafConfig();
        templateEngine = thymeleafConfig.createTemplateEngine();
    }
    
    
@Override 
public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        Context context = new Context();
        String contenido = templateEngine.process("adminuser/setpass", context);
        response.setContentType("text/html");
        response.getWriter().write(contenido);
        






}   


    
@Override 
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
  
    try {
        String passwordant = request.getParameter("passwordant");
        String passwordnueva = request.getParameter("passwordnueva");
        SetPassModel objSetPassModel = new SetPassModel();
        
        if( objSetPassModel.validaPass(passwordant)==true){
             objSetPassModel.setPassAdmin(passwordnueva);
             request.getSession().setAttribute("ESTADO", "ok");
        }else{
            
            response.sendRedirect("messageview/errorpass.html");
        }
    
    
    
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
        Logger.getLogger(SetPassServlet.class.getName()).log(Level.SEVERE, null, ex);
    }
} 


    
    
}
