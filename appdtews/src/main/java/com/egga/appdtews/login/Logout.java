/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.login;

import com.egga.appdtews.include.ThymeleafConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 *
 * @author esteban
 */
public class Logout extends HttpServlet {
    
       private TemplateEngine templateEngine;

 
@Override
    public void init() {
        // Inicialización del TemplateEngine usando ThymeleafConfig
        ThymeleafConfig thymeleafConfig = new ThymeleafConfig();
        templateEngine = thymeleafConfig.createTemplateEngine();
    }


    
@Override 
public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{     
HttpSession sesion = request.getSession();
sesion.setAttribute("logauth",null);
sesion.setAttribute("empresaid",null);
sesion.invalidate();

 Context context = new Context();
 context.setVariable("message", "Por favor, inicia sesión.");
 String contenido = templateEngine.process("loginview/logout", context);
 response.setContentType("text/html");
 response.getWriter().write(contenido);
                



}    
    
}
