/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
 */

package com.egga.appdtews.login;

import com.egga.appdtews.appdte.sii.utilidades.ConfigAppDTE;
import com.egga.appdtews.include.Funciones;
import com.egga.appdtews.include.ThymeleafConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.xml.sax.SAXException;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() {
        // Inicialización del TemplateEngine usando ThymeleafConfig
        ThymeleafConfig thymeleafConfig = new ThymeleafConfig();
        templateEngine = thymeleafConfig.createTemplateEngine();
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String clave = request.getParameter("clave");
        LoginModel objLoginModel = new LoginModel();

        try {
            // Validación de login
            if (objLoginModel.authLogin(login, clave)) {
                request.getSession().setAttribute("login", login);
                request.getSession().setAttribute("loginauth", "yes");

                if (!"admin".equals(login)) {
                    Funciones objFunciones = new Funciones();
                    ConfigAppDTE objConfig = new ConfigAppDTE();

                    try {
                        objFunciones.loadCert(login);
                        String sFichero = objConfig.getPathcert() + login + ".pfx";
                        File fichero = new File(sFichero);

                        if (fichero.exists()) {
                            response.sendRedirect("index");
                        } else {
                            response.sendRedirect("messageview/errorcertificado.html");
                        }
                    } catch (NullPointerException | ParserConfigurationException | SAXException | ClassNotFoundException ex) {
                        Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, "Error al cargar el certificado", ex);
                        response.sendRedirect("messageview/errorcertificado.html");
                    }
                } else {
                    response.sendRedirect("index");
                }
            } else {
                response.sendRedirect("errorlogin");
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, "Error en la autenticación", ex);
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!"yes".equals(request.getSession().getAttribute("loginauth"))) {
            Context context = new Context();
            context.setVariable("message", "Por favor, inicia sesión.");
            
            // Procesamiento de la plantilla Thymeleaf
            try {
                String contenido = templateEngine.process("loginview/login", context);
                response.setContentType("text/html");
                response.getWriter().write(contenido);
            } catch (IOException ex) {
                Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, "Error al procesar la plantilla", ex);
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error al cargar la página de login.");
            }
        } else {
            response.sendRedirect("index");
        }
    }
}

  
  

