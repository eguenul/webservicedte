/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.login;

import com.egga.appdtews.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class SetPassModel {
    
public void setPassAdmin(String password) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{    
    Conexion objConexion = new Conexion();
            objConexion.Conectar();
        
 String sql = "Update Usuario set UsuarioPass='"+ password+  "' where UsuarioLogin='admin'";
 Statement stm = objConexion.getConexion().createStatement();
 stm.execute(sql);
 objConexion.cerrar();
 }   


public boolean validaPass(String password) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
   Conexion objConexion = new Conexion();
   objConexion.Conectar();
        
 String sql = "Select * from Usuario where UsuarioPass='"+ password+  "' and UsuarioLogin='admin'";
 Statement stm = objConexion.getConexion().createStatement();
objConexion.cerrar();
 ResultSet objrecordset =   stm.executeQuery(sql);
boolean flag_pass = objrecordset.next();
return flag_pass;
}




}
