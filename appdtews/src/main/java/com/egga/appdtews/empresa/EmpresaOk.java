/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.empresa;

import com.egga.appdtews.include.ThymeleafConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 *
 * @author esteban
 */
public class EmpresaOk extends HttpServlet {
    
    private TemplateEngine templateEngine;

   @Override
   public void init() {
        // Inicialización del TemplateEngine usando ThymeleafConfig
        ThymeleafConfig thymeleafConfig = new ThymeleafConfig();
        templateEngine = thymeleafConfig.createTemplateEngine();
    
   }
       
@Override
 public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

     
        Context context = new Context();
        /*
        context.setVariable("arraylistusuario",arraylistusuario );
        context.setVariable("objUsuario",objUsuario);   
        context.setVariable("estado","NULL");   
        context.setVariable("urlAccion","addusuario");   
          */     
         
        String contenido = templateEngine.process("/empresaview/empresaok", context);
        response.setContentType("text/html");
        response.getWriter().write(contenido);
        
     
 }
    
}
