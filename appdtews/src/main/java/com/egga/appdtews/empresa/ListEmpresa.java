/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.empresa;

import com.egga.appdtews.include.ThymeleafConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xml.sax.SAXException;



/**
 *
 * @author esteban
 */
public class ListEmpresa extends HttpServlet {
private TemplateEngine templateEngine;

@Override
public void init() {
        // Inicialización del TemplateEngine usando ThymeleafConfig
        ThymeleafConfig thymeleafConfig = new ThymeleafConfig();
        templateEngine = thymeleafConfig.createTemplateEngine();
}
        
    
    
    
    
    
    
    
    
    
    
@Override
 public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 
    try {
        EmpresaModel objEmpresaModel = new EmpresaModel();
        ArrayList<Empresa> arraylistempresa = objEmpresaModel.listEmpresa2();
        
        Context context = new Context();
        context.setVariable("arraylistempresa",arraylistempresa );
        String contenido = templateEngine.process("/empresaview/listempresa", context);
        response.setContentType("text/html");
        response.getWriter().write(contenido);
            
        
    
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
        Logger.getLogger(ListEmpresa.class.getName()).log(Level.SEVERE, null, ex);
    }
  
 
 }
    
    
    
}
