/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.empresa;

import jakarta.servlet.ServletContext;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
@WebServlet(urlPatterns = "/empresa2")
public class EmpresaServlet2 extends HttpServlet{
    


@Override
 public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

     if(request.getSession().getAttribute("loginauth")==null){
         request.getRequestDispatcher("login").forward(request, response); 
       }
 
    try {
        
      
        EmpresaModel objEmpresaModel = new EmpresaModel();
        ArrayList<Empresa> arraylistempresa = objEmpresaModel.listEmpresa();
        request.getSession().setAttribute("arraylistempresa", arraylistempresa);
        getServletConfig().getServletContext().getRequestDispatcher("/empresaview/selectempresa.jsp").forward(request,response);
    } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
        Logger.getLogger(EmpresaServlet2.class.getName()).log(Level.SEVERE, null, ex);
    }
      

 }

@Override
 public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 
    try {
        EmpresaModel objEmpresaModel = new EmpresaModel();
        int empresaid = Integer.parseInt(request.getParameter("EmpresaId"));
        Empresa objEmpresa = objEmpresaModel.getData(empresaid);
        request.getSession().setAttribute("Empresa",objEmpresa);
        request.getSession().setAttribute("empresaid", empresaid);    
        ServletContext context = getServletContext();
        // Obtener el contexto raíz como una cadena de texto
        String contextPath = context.getContextPath();
        response.sendRedirect(contextPath+"/index");
   } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
        Logger.getLogger(EmpresaServlet2.class.getName()).log(Level.SEVERE, null, ex);
    }
 }

}
