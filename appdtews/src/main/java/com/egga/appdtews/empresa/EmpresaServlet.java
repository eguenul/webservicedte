/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
package com.egga.appdtews.empresa;

import com.egga.appdtews.include.ThymeleafConfig;
import com.egga.appdtews.include.comonFunc;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xml.sax.SAXException;
@WebServlet(urlPatterns = "/empresa")
public class EmpresaServlet extends HttpServlet {
   
   private TemplateEngine templateEngine;

   @Override
   public void init() {
        // Inicialización del TemplateEngine usando ThymeleafConfig
        ThymeleafConfig thymeleafConfig = new ThymeleafConfig();
        templateEngine = thymeleafConfig.createTemplateEngine();
    }
     
 @Override
 public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     try {
         String acc = request.getParameter("ACC");
         
           Empresa objEmpresa = new Empresa();
           
           EmpresaModel objEmpresaModel = new EmpresaModel();
         switch(acc){
             
             case "GRABAR":
                 
                        
             case "UPDATE":
                            
             case "LISTADO":
                                ArrayList<Empresa> arraylistempresa = objEmpresaModel.listEmpresa2();
                                request.getSession().setAttribute("arraylistempresa", arraylistempresa);
                                getServletConfig().getServletContext().getRequestDispatcher("/empresaview/listempresa2.jsp").forward(request,response);
                 
                        
                 break;
                 
             case "BUSCAR":
                            request.getSession().setAttribute("estado", null);
                 
                         
                           
                            objEmpresa = objEmpresaModel.searchEmpresa(Integer.parseInt(request.getParameter("EmpresaCod")));
                          
                            request.getSession().setAttribute("acc", "UPDATE");                
                            request.getSession().setAttribute("Empresa", objEmpresa);
                            getServletConfig().getServletContext().getRequestDispatcher("/empresaview/formempresa.jsp").forward(request,response);
                            break;
                            
                            
             case "BUSQUEDACOD":
                                  if(request.getParameter("EmpresaCod").isEmpty()==true){
           
                                }else{
                                        int empresacod = Integer.parseInt(request.getParameter("EmpresaCod"));
                                        arraylistempresa = objEmpresaModel.searchCod(empresacod);
                                       request.getSession().setAttribute("arraylistempresa", arraylistempresa);
                                       getServletConfig().getServletContext().getRequestDispatcher("/empresaview/listempresa2.jsp").forward(request,response);
                                      } 
                                 break;
                            
             case "BUSQUEDARAZ":
                                if(request.getParameter("EmpresaRaz").isEmpty()==true){
           
                                 }else{
                                    String empresarazraz = request.getParameter("EmpresaRaz");
                                    arraylistempresa = objEmpresaModel.searchRaz(empresarazraz);
                                    request.getSession().setAttribute("arraylistempresa", arraylistempresa);
                                    getServletConfig().getServletContext().getRequestDispatcher("/empresaview/listempresa2.jsp").forward(request,response);
                                  }     
                                break;
                            
             case "BUSQUEDARUT":
                                  if(request.getParameter("EmpresaRut").isEmpty()==true){
           
                                    }else{
                                            String empresarut = request.getParameter("EmpresaRut");

                                             if(comonFunc.validaRut(empresarut.trim())==true){
                                                
                                                 arraylistempresa = objEmpresaModel.searchRut(empresarut);
                                                request.getSession().setAttribute("arraylistempresa", arraylistempresa);
                                                getServletConfig().getServletContext().getRequestDispatcher("/empresaview/listempresa2.jsp").forward(request,response);
                                             }else{
                                                 response.setContentType("text/html");

                                                //ya podemos enviar al navegador
                                                try ( //Objetemos el escritor hacia el Cliente
                                                        PrintWriter out = response.getWriter()) {
                                                    //ya podemos enviar al navegador
                                                    String mensaje = "<div class=\"alert alert-danger\"> <strong>ERROR</strong> RUT NO VALIDO</div>";
                                                    out.println(mensaje);
                                                }
                                             }

            
            
            
                                     } 
                 
                                  break;
                            
                            
                            
                            
                            
                            
                            
         }
     } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
           Logger.getLogger(EmpresaServlet.class.getName()).log(Level.SEVERE, null, ex);
     }
 
 
 }

@Override
 public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


       request.getSession().setAttribute("estado", "");
       Empresa objEmpresa = new Empresa();
       objEmpresa.setEmpresaacteco(1);
       objEmpresa.setEmpresaraz("");
       objEmpresa.setEmpresagir("");
       objEmpresa.setEmpresarut("");
       objEmpresa.setEmpresadir("");
       objEmpresa.setEmpresaciu("");
       objEmpresa.setEmpresacom("");
       objEmpresa.setEmpresafon("");
       objEmpresa.setEmpresaema("");
       objEmpresa.setEmpresafechresol("");
       objEmpresa.setSucursalsiicod(1);
       objEmpresa.setEmpresanumresol(0);
       objEmpresa.setOficinasii("");
       request.getSession().setAttribute("Empresa", objEmpresa);
       request.getSession().setAttribute("acc", "GRABAR");
     
        Context context = new Context();
        /*
        context.setVariable("arraylistusuario",arraylistusuario );
        context.setVariable("objUsuario",objUsuario);   
        context.setVariable("estado","NULL");   
        context.setVariable("urlAccion","addusuario");   
          */     
         
        String contenido = templateEngine.process("/empresaview/addempresa", context);
        response.setContentType("text/html");
        response.getWriter().write(contenido);
        
     
     
     
     
     }

 
 
 
 
}