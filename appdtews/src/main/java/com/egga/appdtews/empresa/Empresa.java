/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.empresa;

/**
 *
 * @author esteban
 */
public class Empresa {
    
   private int empresacod;
   private String empresarut;
   private String empresadir;
   private String empresafon;
   private String empresagir;
   private String empresacom;
   private String empresaciu;
   private String empresaraz;
   private int empresaid;
   private String empresaema;
   private int empresaacteco;
   private String empresafechresol;
   private int sucursalsiicod;
   private String oficinasii;

    public int getSucursalsiicod() {
        return sucursalsiicod;
    }

    public void setSucursalsiicod(int sucursalsiicod) {
        this.sucursalsiicod = sucursalsiicod;
    }
   
   
    public int getEmpresaacteco() {
        return empresaacteco;
    }

    public void setEmpresaacteco(int empresaacteco) {
        this.empresaacteco = empresaacteco;
    }

    public String getEmpresafechresol() {
        return empresafechresol;
    }

    public void setEmpresafechresol(String empresafechresol) {
        this.empresafechresol = empresafechresol;
    }

    public int getEmpresanumresol() {
        return empresanumresol;
    }

    public void setEmpresanumresol(int empresanumresol) {
        this.empresanumresol = empresanumresol;
    }
   private int empresanumresol;


    public String getEmpresaema() {
        return empresaema;
    }

    public void setEmpresaema(String empresaema) {
        this.empresaema = empresaema;
    }
    public String getEmpresaciu() {
        return empresaciu;
    }

    public int getEmpresacod() {
        return empresacod;
    }

    public String getEmpresacom() {
        return empresacom;
    }

    public String getEmpresadir() {
        return empresadir;
    }

    public String getEmpresafon() {
        return empresafon;
    }

    public String getEmpresagir() {
        return empresagir;
    }

    public String getEmpresarut() {
        return empresarut;
    }

    public void setEmpresaciu(String empresaciu) {
        this.empresaciu = empresaciu;
    }

    public void setEmpresacod(int empresacod) {
        this.empresacod = empresacod;
    }

    public void setEmpresacom(String empresacom) {
        this.empresacom = empresacom;
    }

    public void setEmpresadir(String empresadir) {
        this.empresadir = empresadir;
    }

    public void setEmpresafon(String empresafon) {
        this.empresafon = empresafon;
    }

    public void setEmpresagir(String empresagir) {
        this.empresagir = empresagir;
    }

    public void setEmpresarut(String empresarut) {
        this.empresarut = empresarut;
    }

    public String getEmpresaraz() {
        return empresaraz;
    }

    public void setEmpresaraz(String empresaraz) {
        this.empresaraz = empresaraz;
    }

    public void setEmpresaid(int empresaid){
        this.empresaid = empresaid;
       
    }

    public int getEmpresaid(){
        return empresaid;
        
    }

    public String getOficinasii() {
        return oficinasii;
    }

    public void setOficinasii(String oficinasii) {
        this.oficinasii = oficinasii;
    }
    
    
    
    
    
}
