/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.empresa;

import com.egga.appdtews.include.comonFunc;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class AddEmpresa extends HttpServlet {
   
 @Override
 public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            Empresa objEmpresa = new Empresa();
            EmpresaModel objEmpresaModel = new EmpresaModel();
            objEmpresa.setEmpresarut(request.getParameter("EmpresaRut"));
            
            
            
            objEmpresa.setEmpresaraz(request.getParameter("EmpresaRaz"));
            objEmpresa.setEmpresarut(request.getParameter("EmpresaRut").trim());
            objEmpresa.setEmpresagir(request.getParameter("EmpresaGir"));
            objEmpresa.setEmpresadir(request.getParameter("EmpresaDir"));
            objEmpresa.setEmpresacom(request.getParameter("EmpresaCom"));
            objEmpresa.setEmpresaciu(request.getParameter("EmpresaCiu"));
            objEmpresa.setEmpresafon(request.getParameter("EmpresaFon"));
            objEmpresa.setEmpresaema(request.getParameter("EmpresaEma"));
                              
            objEmpresa.setEmpresaacteco(Integer.parseInt(request.getParameter("ActEco")));
            objEmpresa.setEmpresanumresol(Integer.parseInt(request.getParameter("ResolSii")));
            objEmpresa.setEmpresafechresol(request.getParameter("FechResol"));
            objEmpresa.setSucursalsiicod(Integer.parseInt(request.getParameter("SucurSiiCod")));
            objEmpresa.setOficinasii(request.getParameter("OficinaSii"));
            
            
            if(comonFunc.validaRut(objEmpresa.getEmpresarut().trim())==true){
                
                objEmpresaModel.addEmpresa(objEmpresa);
                response.sendRedirect("empresaok");
                objEmpresa.setEmpresaraz("");
                objEmpresa.setEmpresarut("");
                objEmpresa.setEmpresagir("");
                objEmpresa.setEmpresadir("");
                objEmpresa.setEmpresacom("");
                objEmpresa.setEmpresaciu("");
                objEmpresa.setEmpresafon("");
                objEmpresa.setEmpresaema("");
                objEmpresa.setEmpresanumresol(0);
                objEmpresa.setEmpresafechresol("");
                objEmpresa.setSucursalsiicod(1);
                objEmpresa.setEmpresacod(0);
                
                
                
                request.getSession().setAttribute("Empresa", objEmpresa);
                request.getSession().setAttribute("estado", "OK");
                
            }else{
                response.sendRedirect("empresaerror");
                
            }
            
            
           
        } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(AddEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        }
                 

 }
}
