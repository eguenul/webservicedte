/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.movimientos;

/**
 *
 * @author esteban
 */
public class Movimiento {
    private int idmovimiento;
    private String fecha;
    private int idtipodoc;
    private int numdoc;
    private int empresaid;
    private String trackid;
    private String totalbruto;
    private String rutemisor;
    private String rutreceptor;
    private String rutenvia;
    private String montoneto;
    private String montoexento;
    private String montoiva;
    private String montonofacturado;
    private String montoperiodo;
    private String tipoOperacion;
    
    
    
 
 
 private String archivonom;
    public int getIdmovimiento() {
        return idmovimiento;
    }

    public void setIdmovimiento(int idmovimiento) {
        this.idmovimiento = idmovimiento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIdtipodoc() {
        return idtipodoc;
    }

    public void setIdtipodoc(int idtipodoc) {
        this.idtipodoc = idtipodoc;
    }

    public int getNumdoc() {
        return numdoc;
    }

    public void setNumdoc(int numdoc) {
        this.numdoc = numdoc;
    }

    public int getEmpresaid() {
        return empresaid;
    }

    public void setEmpresaid(int empresaid) {
        this.empresaid = empresaid;
    }

    public String getTrackid() {
        return trackid;
    }

    public void setTrackid(String trackid) {
        this.trackid = trackid;
    }

    public String getTotalbruto() {
        return totalbruto;
    }

    public void setTotalbruto(String totalbruto) {
        this.totalbruto = totalbruto;
    }

    public String getArchivonom() {
        return archivonom;
    }

    public void setArchivonom(String archivonom) {
        this.archivonom = archivonom;
    }

    public String getRutemisor() {
        return rutemisor;
    }

    public void setRutemisor(String rutemisor) {
        this.rutemisor = rutemisor;
    }

    public String getRutreceptor() {
        return rutreceptor;
    }

    public void setRutreceptor(String rutreceptor) {
        this.rutreceptor = rutreceptor;
    }

    public String getRutenvia() {
        return rutenvia;
    }

    public void setRutenvia(String rutenvia) {
        this.rutenvia = rutenvia;
    }

    public String getMontoneto() {
        return montoneto;
    }

    public void setMontoneto(String montoneto) {
        this.montoneto = montoneto;
    }

    public String getMontoexento() {
        return montoexento;
    }

    public void setMontoexento(String montoexento) {
        this.montoexento = montoexento;
    }

    public String getMontoiva() {
        return montoiva;
    }

    public void setMontoiva(String montoiva) {
        this.montoiva = montoiva;
    }

    public String getMontonofacturado() {
        return montonofacturado;
    }

    public void setMontonofacturado(String montonofacturado) {
        this.montonofacturado = montonofacturado;
    }

    public String getMontoperiodo() {
        return montoperiodo;
    }

    public void setMontoperiodo(String montoperiodo) {
        this.montoperiodo = montoperiodo;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }
    

}
