/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.movimientos;

import com.egga.appdtews.include.Conexion;
import com.egga.appdtews.appdte.sii.utilidades.ConfigAppDTE;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class MovimientoModel {
    
 public void addDTE(Movimiento objMovimiento) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
      deleteDTE(objMovimiento); 
      Conexion objConexion = new Conexion();
      objConexion.Conectar();
      
      
      if("V".equals(objMovimiento.getTipoOperacion())){  
          objMovimiento.setMontonofacturado("0");
          objMovimiento.setMontoperiodo("0");
      }
      
     if("C".equals(objMovimiento.getTipoOperacion())){  
          objMovimiento.setMontoexento("0");
          objMovimiento.setTotalbruto("0");
      }
      
      String sql = "INSERT INTO Movimiento (MovimientoFecha, TipoDocumentoId,TipoOperacion,MontoNeto,MontoExento,MontoIva, MontoTotal,MontoNoFacturado,MontoPeriodo , NumDoc,EmpresaId, MovimientoIdentificadorEnvio, ArchivoNom ,RutEmisor,RutReceptor,RutEnvia) \n";
      sql = sql +" values('" +"\n";
      sql = sql + String.valueOf(objMovimiento.getFecha()) + "',\n";
      sql = sql + String.valueOf(objMovimiento.getIdtipodoc()) + ",\n";
      sql = sql + "'" + objMovimiento.getTipoOperacion() + "',\n";
      sql = sql + String.valueOf(objMovimiento.getMontoneto()) + ",\n";
      sql = sql + String.valueOf(objMovimiento.getMontoexento()) + ",\n";
      sql = sql + String.valueOf(objMovimiento.getMontoiva()) + ",\n";
  
           
      sql = sql + String.valueOf(objMovimiento.getTotalbruto()) + ",\n";
      sql = sql + String.valueOf(objMovimiento.getMontonofacturado()) + ",\n";
      sql = sql + String.valueOf(objMovimiento.getMontoperiodo()) + ",\n";
      sql = sql + String.valueOf(objMovimiento.getNumdoc()) + ",\n";
      sql = sql + String.valueOf(objMovimiento.getEmpresaid()) + ",\n";
      sql = sql + String.valueOf("'"+objMovimiento.getTrackid()) + "',' \n";
      sql = sql + String.valueOf(objMovimiento.getArchivonom()) + "',' \n";
      sql = sql + String.valueOf(objMovimiento.getRutemisor()) + "',' \n";
      sql = sql + String.valueOf(objMovimiento.getRutreceptor()) + "',' \n";
      sql = sql + String.valueOf(objMovimiento.getRutenvia()) + "') \n";
   
      
      
      
      System.out.print(sql);
      Statement stm = objConexion.getConexion().createStatement(); 
      stm.execute(sql);
      objConexion.cerrar();
      addBLOB(objMovimiento);  
 } 
 
 
 public void deleteDTE(Movimiento objMovimiento) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
     Conexion objConexion = new Conexion();
     objConexion.Conectar();
     String sql = "DELETE FROM Movimiento where TipoDocumentoId="+ String.valueOf(objMovimiento.getIdtipodoc())+" and NumDoc="+String.valueOf(objMovimiento.getNumdoc())+" and EmpresaId="+String.valueOf(objMovimiento.getEmpresaid());
     Statement stm = objConexion.getConexion().createStatement(); 
     stm.execute(sql);
     objConexion.cerrar();
 }
 
 public void addBLOB(Movimiento objMovimiento){
     try {
         System.setProperty("file.encoding", "ISO-8859-1");
         Conexion objConexion = new Conexion();
         objConexion.Conectar();
         ConfigAppDTE objconfig = new ConfigAppDTE();
         FileInputStream input = null;
         String sql = "Update Movimiento SET BlobDTE=? where TipoDocumentoId=? and NumDoc=? and EmpresaId=?";
         PreparedStatement stm = objConexion.getConexion().prepareStatement(sql);
         input = new FileInputStream(new File(objconfig.getPathdte()+objMovimiento.getArchivonom()));
         stm.setBinaryStream(1, input);
         stm.setInt(2, objMovimiento.getIdtipodoc());
         stm.setInt(3, objMovimiento.getNumdoc());
         stm.setInt(4, objMovimiento.getEmpresaid());
         stm.execute();
         objConexion.cerrar();
     } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
         Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
     }
 }
    
public int getIdMovimiento(int tipodocid, int folio, int empresaid) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
  
   String sql = "Select * from Movimiento where TipoDocumentoId="+String.valueOf(tipodocid) + " and NumDoc="+folio + " and EmpresaId=" + String.valueOf(empresaid);
   Conexion objConexion = new Conexion();
   objConexion.Conectar();
   System.out.print(sql);
   Statement stm = objConexion.getConexion().createStatement();
   ResultSet objRecordset = stm.executeQuery(sql);
   int idmovimiento =0;
   while(objRecordset.next()){ 
    idmovimiento = objRecordset.getInt("MovimientoId");
   }
    objConexion.cerrar();
   return idmovimiento;
}    


public int  conteoMovimiento(String fechadesde, String fechahasta,int empresaid) throws SQLException{
    try {
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        
        
        
        
        
        String fecha1 = fechadesde;
        
        
        
        
        String fecha2 = fechahasta;
        
        
        String sql ="select count(*) as NroRegistros  \n"+
                "from Movimiento \n"+
                "inner join TipoDocumentos on TipoDocumentos.TipoDocumentoId = Movimiento.TipoDocumentoId \n"+
                "inner join CliProv on CliProv.CliProvId = Movimiento.CliProvId \n"+
                "where (TipoDocumentos.CodigoSii<> 801 and TipoDocumentos.CodigoSii<> 802 and TipoDocumentos.CodigoSii<> 0) and \n";
        
        if(fecha1.equals(fecha2)){
            sql= sql+" CliProv.EmpresaId="+String.valueOf(empresaid) + " and MovimientoFecha='"+fecha1+"'";
        }else{
            
            sql= sql+" MovimientoFecha between DATE_FORMAT('"+fecha1+"','%Y-%m-%d') and" +  " DATE_FORMAT('"+fecha2+"','%Y-%m-%d') and CliProv.EmpresaId="+String.valueOf(empresaid);
        }
        System.out.print(sql);
        Statement stm = objConexion.getConexion().createStatement();
        ResultSet objrecordset = stm.executeQuery(sql);
        int nroregistro = 0;
        while(objrecordset.next()){
            nroregistro =  objrecordset.getInt("NroRegistros");
        }
        objConexion.cerrar();
        return nroregistro;
    } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
    }
    return 0;
    

}  




public ArrayList<Object[]> listFecha(String fechadesde, String fechahasta,int empresaid,int indice) throws SQLException{
    try {
        String sql;
        
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
        
        String fecha1 =fechadesde;
        
        
        
        
        String fecha2 = fechahasta;
        
        
        
        sql ="Select DATE_FORMAT(Movimiento.MovimientoFecha,'%d-%m-%Y') as MovimientoFecha  ,\n"+
                "TipoDocumentos.TipoDocumentoDes,TipoDocumentos.CodigoSii, \n"+
                "Movimiento.NumDoc,Movimiento.MovimientoTotalBruto,Movimiento.MovimientoId, \n"+
                "Movimiento.MovimientoIdentificadorEnvio,\n"+
                "CliProv.CliProvRut, CliProv.CliProvCod , CliProv.EmpresaId \n"+
                "from Movimiento \n"+
                "inner join TipoDocumentos on TipoDocumentos.TipoDocumentoId = Movimiento.TipoDocumentoId \n"+
                "inner join CliProv on CliProv.CliProvId = Movimiento.CliProvId \n"+
                 "where (TipoDocumentos.CodigoSii<> 801 and TipoDocumentos.CodigoSii<> 802 and TipoDocumentos.CodigoSii<> 0) and \n";
        if(fecha1.equals(fecha2)){
            sql= sql+" MovimientoFecha='"+fecha1+ "' and CliProv.EmpresaId="+String.valueOf(empresaid)+" limit "+String.valueOf(indice)+",10";
        }else{
            
            sql= sql+" MovimientoFecha between DATE_FORMAT('"+fecha1+"','%Y-%m-%d') and" +  " DATE_FORMAT('"+fecha2+"','%Y-%m-%d') and CliProv.EmpresaId="+String.valueOf(empresaid)+" limit "+String.valueOf(indice)+",10";
        }
        
        System.out.print(sql);
        Statement stm = objConexion.getConexion().createStatement();
        ResultSet objrecordset = stm.executeQuery(sql);
        ArrayList<Object[]> arraymovimientos = new ArrayList<>();
        while(objrecordset.next()){
            Object[] objMovimiento = new Object[9];
            objMovimiento[0] = objrecordset.getString("TipoDocumentoDes");
            objMovimiento[1] = objrecordset.getInt("NumDoc");
            objMovimiento[2] =   objrecordset.getString("MovimientoFecha");
            objMovimiento[3]  =  objrecordset.getInt("MovimientoTotalBruto");
            objMovimiento[4]  =  objrecordset.getInt("MovimientoIdentificadorEnvio");
            objMovimiento[5]  =  objrecordset.getString("CliProvRut");
            objMovimiento[6]  =  objrecordset.getString("MovimientoId");
            objMovimiento[7]  =  objrecordset.getString("CodigoSii");
            objMovimiento[8]  =  objrecordset.getString("CliProvCod");
            
            arraymovimientos.add(objMovimiento);
        }
        System.out.print(sql);
        objConexion.cerrar();
        return arraymovimientos;
    } catch (ClassNotFoundException | ParserConfigurationException | SAXException | IOException ex) {
        Logger.getLogger(MovimientoModel.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
}


}
