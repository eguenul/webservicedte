/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.filters;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;


public class AuthFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Inicialización del filtro (si es necesario)
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        // Verifica si hay una sesión válida con "loginauth"
        if (httpRequest.getSession().getAttribute("loginauth") == null) {
            // Redirige al login si no está autenticado
            httpResponse.sendRedirect(httpRequest.getContextPath() + "/login");
        } else {
            // Si está autenticado, continúa la cadena de filtros o va al servlet
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        // Limpieza cuando se destruye el filtro (si es necesario)
    }
}
