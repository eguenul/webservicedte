/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/

package com.egga.appdtews.admincaf;

import com.egga.appdtews.include.ThymeleafConfig;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
@WebServlet(urlPatterns = "/adminCAF")
public class AdminCaf extends HttpServlet {
     private TemplateEngine templateEngine;

   

     @Override
    public void init() {
        // Inicialización del TemplateEngine usando ThymeleafConfig
        ThymeleafConfig thymeleafConfig = new ThymeleafConfig();
        templateEngine = thymeleafConfig.createTemplateEngine();
    }
        
    
    
    
    
    
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  
      try {
          int empresaid =  (int) request.getSession().getAttribute("empresaid");
          
          listCAF objlistCAF = new listCAF();
          
        ArrayList<Object[]> arraylistcaf = objlistCAF.listCAF(empresaid);
        request.getSession().setAttribute("arraylistcaf", arraylistcaf);
          
        Context context = new Context();
        context.setVariable("arraylistcaf",arraylistcaf );
           
                
         
        String contenido = templateEngine.process("/admincafview/adminCAF", context);
        response.setContentType("text/html");
        response.getWriter().write(contenido);
        
        
        
          
       } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
          Logger.getLogger(AdminCaf.class.getName()).log(Level.SEVERE, null, ex);
      }
  
  }   
    
  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  

  }   
    
    
}
