/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
package com.egga.appdtews.admincaf;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import com.egga.appdtews.appdte.sii.utilidades.ConfigAppDTE;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(urlPatterns = "/uploadCAF")
@MultipartConfig // Necesario para manejar archivos en partes (sin librerías externas)
public class UploadCaf extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String login = (String) request.getSession().getAttribute("login");
            ConfigAppDTE objConfig = new ConfigAppDTE();
            String pathCAF = objConfig.getPathcaf();
            String fileName = "";
            
            try {
                // Obtener la parte que contiene el archivo subido
                Part filePart = request.getPart("file"); // El campo "name" del input file debe ser "file"
                if (filePart != null) {
                    fileName = getFileName(filePart);
                    File uploadedFile = new File(pathCAF + login + fileName);
                    
                    // Guardar el archivo en el sistema de archivos
                    filePart.write(uploadedFile.getAbsolutePath());
                    System.out.println("Archivo subido: " + fileName);
                    
                    // Procesar el archivo subido y guardarlo en la base de datos
                    int empresaId = (int) request.getSession().getAttribute("empresaid");
                    saveCAf saveCAFInstance = new saveCAf();
                    
                    if (!saveCAFInstance.readCAF(empresaId, uploadedFile.getAbsolutePath())) {
                        response.sendRedirect("errorcaf");
                    } else {
                        saveCAFInstance.guardarCAF(empresaId, uploadedFile.getAbsolutePath());
                         response.sendRedirect("cafok");
                    }
                } else {
                    request.setAttribute("message", "No se ha subido ningún archivo.");
                    response.sendRedirect("/AppDTEWS/messageview/error.html");
                }
            } catch (IllegalStateException | ParserConfigurationException | SAXException | SQLException | ClassNotFoundException ex) {
                Logger.getLogger(UploadCaf.class.getName()).log(Level.SEVERE, null, ex);
                response.sendRedirect("/AppDTEWS/messageview/error.html");
            }
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(UploadCaf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Método auxiliar para obtener el nombre del archivo subido
    private String getFileName(Part part) {
        String contentDisposition = part.getHeader("content-disposition");
        for (String token : contentDisposition.split(";")) {
            if (token.trim().startsWith("filename")) {
                return token.substring(token.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
}
