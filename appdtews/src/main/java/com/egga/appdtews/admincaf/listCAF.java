/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
package com.egga.appdtews.admincaf;

import com.egga.appdtews.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class listCAF {
    
    public ArrayList<Object[]> listCAF(int empresaid) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
         Conexion objConexion = new Conexion();
        objConexion.Conectar();
        
        
         String sql ="Select CAF.idCAF, CAF.Desde, CAF.Hasta, TipoDocumentos.TipoDocumentoDes from CAF \n" +
        "INNER join TipoDocumentos on CAF.TipoCaf = TipoDocumentos.TipoDocumentoId where CAF.EmpresaId="+String.valueOf(empresaid); 
            
        Statement stm = objConexion.getConexion().createStatement();
        ResultSet objrecordset = stm.executeQuery(sql);
      
        
         ArrayList<Object[]> arraylistcaf = new ArrayList<>();
        
         
   while (objrecordset.next()){
         Object[] auxData = new Object[4];
       auxData[0] = objrecordset.getInt("idCAF");
       auxData[1] = objrecordset.getInt("Desde");
       auxData[2] = objrecordset.getInt("Hasta");
       auxData[3] = objrecordset.getString("TipoDocumentoDes");
       arraylistcaf.add(auxData);
        }
        objConexion.cerrar();
        return arraylistcaf;
    }
    
    
    
    
    
}
