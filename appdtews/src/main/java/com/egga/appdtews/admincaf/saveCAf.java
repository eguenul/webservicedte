/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
package com.egga.appdtews.admincaf;

import com.egga.appdtews.documento.DocumentoModel;
import com.egga.appdtews.empresa.Empresa;
import com.egga.appdtews.empresa.EmpresaModel;
import com.egga.appdtews.include.Conexion;
import com.egga.appdtews.appdte.sii.utilidades.getBytesCAF;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class saveCAf {


public boolean readCAF(int empresaid , String archivo) throws SQLException, ClassNotFoundException{
  
    try{
        
        String filepath2 = archivo;
	
        DocumentBuilderFactory docFactory2 = DocumentBuilderFactory.newInstance();
	DocumentBuilder docBuilder2 = docFactory2.newDocumentBuilder();
	Document doc2 = docBuilder2.parse(filepath2);
   
        NodeList nl = doc2.getElementsByTagName("RE");
        Element el = (Element) nl.item(0);
        String rutemisor = el.getFirstChild().getNodeValue();
       
        EmpresaModel objEmpresaModel = new EmpresaModel();    
        Empresa objEmpresa = objEmpresaModel.getData(empresaid);
        
   
        
        
        
        return rutemisor.trim().equals(objEmpresa.getEmpresarut().trim());
        
        
        
    }catch(IOException | ParserConfigurationException | DOMException | SAXException ex){
        return false;
        
    }
    
    
}

public void  guardarCAF(int empresaid , String archivo) throws ParserConfigurationException, SAXException, SQLException, IOException, ClassNotFoundException{   
   
    String filepath2 = archivo;
   
	
    DocumentBuilderFactory docFactory2 = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder2 = docFactory2.newDocumentBuilder();
    Document doc2 = docBuilder2.parse(filepath2);
   
    NodeList nl = doc2.getElementsByTagName("RE");
    Element el = (Element) nl.item(0);
        
     
    String rutemisor = el.getFirstChild().getNodeValue();
     
     
    
    NodeList nodotd = doc2.getElementsByTagName("TD");
    Element eltd = (Element) nodotd.item(0);
    String tipodte = eltd.getFirstChild().getNodeValue();
        
    NodeList nodod = doc2.getElementsByTagName("D");
    Element eld = (Element) nodod.item(0);
    String desde = eld.getFirstChild().getNodeValue();
        
        
    NodeList nodoh = doc2.getElementsByTagName("H");
    Element elh = (Element) nodoh.item(0);
    String hasta = elh.getFirstChild().getNodeValue();
        
    
    DocumentoModel objDocumentoModel = new DocumentoModel();
    
    int idTipoDocumento = objDocumentoModel.getId(tipodte.trim());
    
    getBytesCAF objGetBytes = new getBytesCAF();
    
    
    Conexion objconexion = new Conexion();
     objconexion.Conectar();
    
            
    byte[] bkey = objGetBytes.getBytesCAF(filepath2);
     
    String query = "INSERT INTO CAF (EmpresaId, TipoCaf, Desde, Hasta,BlobCAF) VALUES (?,?,?,?,?)";
    
    PreparedStatement pstmt = objconexion.getConexion().prepareStatement(query);
    pstmt.setInt(1, empresaid);
    pstmt.setInt(2, idTipoDocumento);
    pstmt.setInt(3, Integer.parseInt(desde.trim()));
    pstmt.setInt(4, Integer.parseInt(hasta.trim()));
    pstmt.setBytes(5, bkey);
    
    pstmt.execute();
    objconexion.cerrar();

    
    File fichero = new File(filepath2);   
    if (fichero.delete())
   System.out.println("El fichero ha sido borrado satisfactoriamente");
else
   System.out.println("El fichero no puede ser borrado");
    
 
}


}
