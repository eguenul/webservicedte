/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.appdte.json;

/**
 *
 * @author esteban
 */
public class TrackID {
    
    /*
    
    {
"rut_emisor": "76040308-3",
"rut_envia": "13968481-8",
"trackid": 19658149,
"fecha_recepcion": "2022-10-21 11:37:21",
"estado": "REC",
"file": "ENVDTE76040308F9T39.xml"
}
    
    */
    
private String rut_emisor;
private String rut_envia;
private String trackid;
private String fecha_recepcion;
private String estado;
private String file;
private String mensajerror;

    public String getRut_emisor() {
        return rut_emisor;
    }

    public void setRut_emisor(String rut_emisor) {
        this.rut_emisor = rut_emisor;
    }


    public String getTrackid() {
        return trackid;
    }

    public void setTrackid(String trackid) {
        this.trackid = trackid;
    }

    public String getFecha_recepcion() {
        return fecha_recepcion;
    }

    public void setFecha_recepcion(String fecha_recepcion) {
        this.fecha_recepcion = fecha_recepcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getRut_envia() {
        return rut_envia;
    }

    public void setRut_envia(String rut_envia) {
        this.rut_envia = rut_envia;
    }

    public String getMensajerror() {
        return mensajerror;
    }

    public void setMensajerror(String mensajerror) {
        this.mensajerror = mensajerror;
    }



}
