/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.appdte.json;

/**
 *
 * @author esteban
 */
public class DescGlobalJson {

    private String nrolindr;
    private String tpoMov;
    private String tpovalor;
    private String valordr;
    private String indexedr;
    private String glosadr;

    public String getNrolindr() {
        return nrolindr;
    }

    public void setNrolindr(String nrolindr) {
        this.nrolindr = nrolindr;
    }

    public String getTpoMov() {
        return tpoMov;
    }

    public void setTpoMov(String tpoMov) {
        this.tpoMov = tpoMov;
    }

    public String getTpovalor() {
        return tpovalor;
    }

    public void setTpovalor(String tpovalor) {
        this.tpovalor = tpovalor;
    }

    public String getValordr() {
        return valordr;
    }

    public void setValordr(String valordr) {
        this.valordr = valordr;
    }

    public String getIndexedr() {
        return indexedr;
    }

    public void setIndexedr(String indexedr) {
        this.indexedr = indexedr;
    }

    public String getGlosadr() {
        return glosadr;
    }

    public void setGlosadr(String glosadr) {
        this.glosadr = glosadr;
    }
    
    
    
    
    

}
