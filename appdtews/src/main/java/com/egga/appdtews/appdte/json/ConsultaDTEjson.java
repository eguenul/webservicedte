/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.appdte.json;



public class ConsultaDTEjson {
    
 private UsuarioJson usuario; 
 private DatosDTE datosDTE;   


public ConsultaDTEjson(){
    
    
}

    public DatosDTE getDatosDTE() {
        return datosDTE;
    }

    public void setDatosDTE(DatosDTE datosDTE) {
        this.datosDTE = datosDTE;
    }

    public UsuarioJson getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioJson usuario) {
        this.usuario = usuario;
    }

}
