package com.egga.appdtews.appdte.rest.resources;

import com.egga.appdtews.appdte.json.ConsultaDTEjson;
import com.egga.appdtews.service.DteStatusService;
import com.egga.appdtews.service.ServiceFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import jakarta.ws.rs.core.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/dteSTATUS")
public class dteSTATUS extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(dteSTATUS.class.getName());
    private final DteStatusService dteStatusService = ServiceFactory.createDteStatusService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // Leer el cuerpo de la solicitud (JSON)
            InputStream inputStream = request.getInputStream();
            String json = new String(inputStream.readAllBytes());
            
            // Convertir el JSON a un objeto ConsultaDTEjson
            // Puedes usar una librería como Jackson para hacer esto
            ConsultaDTEjson objConsulta = new ObjectMapper().readValue(json, ConsultaDTEjson.class);

            String result = dteStatusService.getState(objConsulta);

            response.setContentType(MediaType.APPLICATION_JSON);
            response.getWriter().write(result);
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error while getting DTE status", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("{\"error\": \"Ocurrió un error: " + e.getMessage() + "\"}");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            InputStream htmlStream = getClass().getResourceAsStream("/docs/dtesatus.html");
            if (htmlStream == null) {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                response.getWriter().write("Documentation not found.");
            } else {
                response.setContentType(MediaType.TEXT_HTML);
                response.getOutputStream().write(htmlStream.readAllBytes());
                response.setStatus(HttpServletResponse.SC_OK);
            }
        } catch (IOException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Error loading documentation.");
        }
    }
}
