/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.appdte.json;

/**
 *
 * @author esteban
 */
public class CaratulaJson {
private String rutenvia;
private String fchinicio;
private String fchfinal; 
private String secenvio;   

public CaratulaJson(){
    
    
}

    public String getRutenvia() {
        return rutenvia;
    }

    public void setRutenvia(String rutenvia) {
        this.rutenvia = rutenvia;
    }

    public String getFchinicio() {
        return fchinicio;
    }

    public void setFchinicio(String fchinicio) {
        this.fchinicio = fchinicio;
    }

    public String getFchfinal() {
        return fchfinal;
    }

    public void setFchfinal(String fchfinal) {
        this.fchfinal = fchfinal;
    }

    public String getSecenvio() {
        return secenvio;
    }

    public void setSecenvio(String secenvio) {
        this.secenvio = secenvio;
    }
 
}
