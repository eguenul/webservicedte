/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.appdte.json;

import java.util.ArrayList;

/**
 *
 * @author esteban
 */
public class ConsumoJson {
  ArrayList<ResumenJson> resumen;
  EmisorJson emisor;
  ResumenJson objresumen;
  CaratulaJson caratula;

    public CaratulaJson getCaratula() {
        return caratula;
    }

    public void setCaratula(CaratulaJson caratula) {
        this.caratula = caratula;
    }
    public ResumenJson getObjresumen() {
        return objresumen;
    }

    public void setObjresumen(ResumenJson objresumen) {
        this.objresumen = objresumen;
    }
 public ConsumoJson(){
     
     
 }
 
 
    
    public void setEmisor(EmisorJson emisor){
        this.emisor = emisor;
        
    }
    
    public EmisorJson getEmisor(){
        
        return emisor;
    }
    
 
    public ArrayList<ResumenJson> getResumen() {
        return resumen;
    }

    public void setResumen(ArrayList<ResumenJson> resumen) {
        this.resumen = resumen;
    }
    
    
    
    
}
