/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.appdte.json;

   

public class EmisorJson {
    
    
   private String rutemisor;
   private String rznsoc;
   private String giroemis;
   private String acteco;
   private String dirorigen;
   private String cmnaorigen;
   private String ciudadorigen;
    
   
   private String fchresol;
   private String nroresol;
   private String cdgsiisucur;
   
    
    public EmisorJson(){
        
        
    }

    public String getRutemisor() {
        return rutemisor;
    }

    public void setRutemisor(String rutemisor) {
        this.rutemisor = rutemisor;
    }

    public String getRznsoc() {
        return rznsoc;
    }

    public void setRznsoc(String rznsoc) {
        this.rznsoc = rznsoc;
    }

    public String getGiroemis() {
        return giroemis;
    }

    public void setGiroemis(String giroemis) {
        this.giroemis = giroemis;
    }

    public String getActeco() {
        return acteco;
    }

    public void setActeco(String acteco) {
        this.acteco = acteco;
    }

    public String getDirorigen() {
        return dirorigen;
    }

    public void setDirorigen(String dirorigen) {
        this.dirorigen = dirorigen;
    }

    public String getCmnaorigen() {
        return cmnaorigen;
    }

    public void setCmnaorigen(String cmnaorigen) {
        this.cmnaorigen = cmnaorigen;
    }

    public String getCiudadorigen() {
        return ciudadorigen;
    }

    public void setCiudadorigen(String ciudadorigen) {
        this.ciudadorigen = ciudadorigen;
    }

    public String getFchresol() {
        return fchresol;
    }

    public void setFchresol(String fchresol) {
        this.fchresol = fchresol;
    }

    public String getNroresol() {
        return nroresol;
    }

    public void setNroresol(String nroresol) {
        this.nroresol = nroresol;
    }

    public String getCdgsiisucur() {
        return cdgsiisucur;
    }

    public void setCdgsiisucur(String cdgsiisucur) {
        this.cdgsiisucur = cdgsiisucur;
    }
    
   
}
