/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.appdte.json;

/**
 *
 * @author esteban
 */
public class IdDteCesionjson {
private String tipodte;
private String rutemisor;
private String folio;
private String fchemis;
private String mnttotal;
private String rutreceptor;
private String rsreceptor;

    public String getRsreceptor() {
        return rsreceptor;
    }

    public void setRsreceptor(String rsreceptor) {
        this.rsreceptor = rsreceptor;
    }
public String getTipodte() {
        return tipodte;
    }

    public void setTipodte(String tipodte) {
        this.tipodte = tipodte;
    }

    public String getRutemisor() {
        return rutemisor;
    }

    public void setRutemisor(String rutemisor) {
        this.rutemisor = rutemisor;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFchemis() {
        return fchemis;
    }

    public void setFchemis(String fchemis) {
        this.fchemis = fchemis;
    }

    public String getMnttotal() {
        return mnttotal;
    }

    public void setMnttotal(String mnttotal) {
        this.mnttotal = mnttotal;
    }

    public String getRutreceptor() {
        return rutreceptor;
    }

    public void setRutreceptor(String rutreceptor) {
        this.rutreceptor = rutreceptor;
    }

}
