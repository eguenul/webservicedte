/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.appdte.sii.utilidades;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author esteban
 */
public class getBytesCAF {
 
 public byte[] getBytesCAF(String filePath){
     
     try {
         System.setProperty("file.encoding", "ISO-8859-1");
         byte[] bytes = Files.readAllBytes(Paths.get(filePath));
    
         String strDATA = new String(bytes, "ISO-8859-1");
         String strCleaned = strDATA.replaceAll("&#13;", "");
         
         byte[] bytes2 = strCleaned.getBytes("ISO-8859-1");
    
         return bytes2;
     } catch (IOException ex) {
         Logger.getLogger(getBytesCAF.class.getName()).log(Level.SEVERE, null, ex);
     }
     return null;
          
      
     
 }
    
    
}
