/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.appdte.json;


public class ReceptorJson {
    
    
   private String rutrecep;
   private String rznsocrecep;
   private String girorecep;
   private String dirrecep;
   private String cmnarecep;
   private String ciudadrecep;
   private String rutcaratula;

    public String getRutrecep() {
        return rutrecep;
    }

    public void setRutrecep(String rutrecep) {
        this.rutrecep = rutrecep;
    }

    public String getRznsocrecep() {
        return rznsocrecep;
    }

    public void setRznsocrecep(String rznsocrecep) {
        this.rznsocrecep = rznsocrecep;
    }

    public String getGirorecep() {
        return girorecep;
    }

    public void setGirorecep(String girorecep) {
        this.girorecep = girorecep;
    }

    public String getDirrecep() {
        return dirrecep;
    }

    public void setDirrecep(String dirrecep) {
        this.dirrecep = dirrecep;
    }

    public String getCmnarecep() {
        return cmnarecep;
    }

    public void setCmnarecep(String cmnarecep) {
        this.cmnarecep = cmnarecep;
    }

    public String getCiudadrecep() {
        return ciudadrecep;
    }

    public void setCiudadrecep(String ciudadrecep) {
        this.ciudadrecep = ciudadrecep;
    }

    public String getRutcaratula() {
        return rutcaratula;
    }

    public void setRutcaratula(String rutcaratula) {
        this.rutcaratula = rutcaratula;
    }
   
    

    
}
