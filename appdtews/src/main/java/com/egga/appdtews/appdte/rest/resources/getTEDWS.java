package com.egga.appdtews.appdte.rest.resources;

import com.egga.appdtews.appdte.json.PrintJSON;
import com.egga.appdtews.service.ServiceFactory;
import com.egga.appdtews.service.TEDService;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import jakarta.ws.rs.core.*;
import java.io.*;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.xml.sax.SAXException;

@WebServlet("/getTEDWS")
public class getTEDWS extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(getTEDWS.class.getName());
    private final TEDService tedService = ServiceFactory.createTEDService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // Leer el cuerpo de la solicitud (JSON)
            InputStream inputStream = request.getInputStream();
            String json = new String(inputStream.readAllBytes());

            // Convertir el JSON a un objeto PrintJSON
            // Usa Jackson para la conversión
            PrintJSON requestjson = new ObjectMapper().readValue(json, PrintJSON.class);
            String result = tedService.getTED(requestjson);
            response.setContentType(MediaType.TEXT_PLAIN);
            response.getWriter().write(result);
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException | IOException | TransformerException e) {
            LOGGER.log(Level.SEVERE, "Error while tracking boleta", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Error: " + e.getMessage());
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            InputStream htmlStream = getClass().getResourceAsStream("/docs/getted.html");
            if (htmlStream == null) {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                response.getWriter().write("Documentation not found.");
            } else {
                response.setContentType(MediaType.TEXT_HTML);
                response.getOutputStream().write(htmlStream.readAllBytes());
                response.setStatus(HttpServletResponse.SC_OK);
            }
        } catch (IOException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Error loading documentation.");
        }
    }
}
