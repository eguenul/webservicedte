/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.appdte.json;

import java.util.List;

/**
 *
 * @author esteban
 */
public class DetalleDteJson {
private int nrolindet;

private String nmbitem;
private String dscitem;
private String qtyitem;
/*
UnmdRef
*/
private String unmditem;  


private String prcitem;
private String montoitem;
private String descuentopct;
private String descuentomonto;
private String indexe;
private String codimpadic;
private List<VlrCodigoJson> cdgitem;
/*
private  String item1;
*/
public void DetalleDteJson(){
    descuentopct = "0";
    descuentomonto = "0";
    indexe="0";
    unmditem = null;
    codimpadic = null;
}

    public int getNrolindet() {
        return nrolindet;
    }

    public void setNrolindet(int nrolindet) {
        this.nrolindet = nrolindet;
    }

    public String getNmbitem() {
        return nmbitem;
    }

    public void setNmbitem(String nmbitem) {
        this.nmbitem = nmbitem;
    }

    public String getDscitem() {
        return dscitem;
    }

    public void setDscitem(String dscitem) {
        this.dscitem = dscitem;
    }

    public String getQtyitem() {
        return qtyitem;
    }

    public void setQtyitem(String qtyitem) {
        this.qtyitem = qtyitem;
    }

    public String getUnmditem() {
        return unmditem;
    }

    public void setUnmditem(String unmditem) {
        this.unmditem = unmditem;
    }

    public String getPrcitem() {
        return prcitem;
    }

    public void setPrcitem(String prcitem) {
        this.prcitem = prcitem;
    }

    public String getMontoitem() {
        return montoitem;
    }

    public void setMontoitem(String montoitem) {
        this.montoitem = montoitem;
    }

    public String getDescuentopct() {
        return descuentopct;
    }

    public void setDescuentopct(String descuentopct) {
        this.descuentopct = descuentopct;
    }

    public String getDescuentomonto() {
        return descuentomonto;
    }

    public void setDescuentomonto(String descuentomonto) {
        this.descuentomonto = descuentomonto;
    }

    public String getIndexe() {
        return indexe;
    }

    public void setIndexe(String indexe) {
        this.indexe = indexe;
    }

    public String getCodimpadic() {
        return codimpadic;
    }

    public void setCodimpadic(String codimpadic) {
        this.codimpadic = codimpadic;
    }

    public List<VlrCodigoJson> getCdgitem() {
        return cdgitem;
    }

    public void setCdgitem(List<VlrCodigoJson> cdgitem) {
        this.cdgitem = cdgitem;
    }
    
    
   
    
    
    


    
}
