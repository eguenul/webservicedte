/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.appdte.json;



public class IdDteJson {

private String tipodte;
private String folio;
private String fchemis;
private String tipodespacho;
private String indtraslado;
private String fmapago;
private String indservicio;
private String indmntneto;
private String fchvenc;

public void idDteJson(){
    
    
    this. indmntneto="0";
    this.fchvenc = null;
}

    public String getTipodte() {
        return tipodte;
    }

    public void setTipodte(String tipodte) {
        this.tipodte = tipodte;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFchemis() {
        return fchemis;
    }

    public void setFchemis(String fchemis) {
        this.fchemis = fchemis;
    }

    public String getTipodespacho() {
        return tipodespacho;
    }

    public void setTipodespacho(String tipodespacho) {
        this.tipodespacho = tipodespacho;
    }

    public String getIndtraslado() {
        return indtraslado;
    }

    public void setIndtraslado(String indtraslado) {
        this.indtraslado = indtraslado;
    }

    public String getFmapago() {
        return fmapago;
    }

    public void setFmapago(String fmapago) {
        this.fmapago = fmapago;
    }

    public String getIndservicio() {
        return indservicio;
    }

    public void setIndservicio(String indservicio) {
        this.indservicio = indservicio;
    }

    public String getIndmntneto() {
        return indmntneto;
    }

    public void setIndmntneto(String indmntneto) {
        this.indmntneto = indmntneto;
    }

    public String getFchvenc() {
        return fchvenc;
    }

    public void setFchvenc(String fchvenc) {
        this.fchvenc = fchvenc;
    }

  

}
