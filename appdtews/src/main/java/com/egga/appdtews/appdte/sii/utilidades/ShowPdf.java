/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/

package com.egga.appdtews.appdte.sii.utilidades;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import javax.swing.JOptionPane;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;



public class ShowPdf{
    
    
    public ShowPdf(){
        
    }
    

    public void showPdf (String rutemisor,String foliodte,String tipodte) throws ParserConfigurationException, SAXException, IOException {

        
       String[] arrayrutemisor = rutemisor.split("-");
       rutemisor = arrayrutemisor[0];
       String nombredte = "DTE"+rutemisor+"F"+foliodte+"T"+tipodte;
     
        
        
         ConfigAppDTE objconfiguracion = new ConfigAppDTE();
         String pathpdf = objconfiguracion.getPathpdf();
          File pdffile = new File(pathpdf+nombredte+".pdf");
           try {
               Desktop.getDesktop().open(pdffile);
           } catch (Exception ex) {
              JOptionPane.showMessageDialog(null, "Unable to load the help file", "File Read Error", JOptionPane.ERROR_MESSAGE);
           }

         }
    
    
}