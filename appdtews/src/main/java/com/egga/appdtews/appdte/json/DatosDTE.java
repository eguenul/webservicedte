/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.appdte.json;

/**
 *
 * @author esteban
 */
public class DatosDTE {
    
private String rutconsultante; 
private String dvconsultante;
private String rutcompania;
private String dvcompania;
private String rutreceptor;
private String dvreceptor;
private String tipodte;
private String foliodte;
private String fechaemisiondte;
private String montodte;    


public DatosDTE(){
    
    
}

    public String getRutconsultante() {
        return rutconsultante;
    }

    public void setRutconsultante(String rutconsultante) {
        this.rutconsultante = rutconsultante;
    }

   
    
    

    public String getRutcompania() {
        return rutcompania;
    }

    public void setRutcompania(String rutcompania) {
        this.rutcompania = rutcompania;
    }

    public String getDvcompania() {
        return dvcompania;
    }

    public void setDvcompania(String dvcompania) {
        this.dvcompania = dvcompania;
    }

    public String getRutreceptor() {
        return rutreceptor;
    }

    public void setRutreceptor(String rutreceptor) {
        this.rutreceptor = rutreceptor;
    }

    public String getDvreceptor() {
        return dvreceptor;
    }

    public void setDvreceptor(String dvreceptor) {
        this.dvreceptor = dvreceptor;
    }

    public String getTipodte() {
        return tipodte;
    }

    public void setTipodte(String tipodte) {
        this.tipodte = tipodte;
    }

    public String getFoliodte() {
        return foliodte;
    }

    public void setFoliodte(String foliodte) {
        this.foliodte = foliodte;
    }

    public String getFechaemisiondte() {
        return fechaemisiondte;
    }

    public void setFechaemisiondte(String fechaemisiondte) {
        this.fechaemisiondte = fechaemisiondte;
    }

    public String getMontodte() {
        return montodte;
    }

    public void setMontodte(String montodte) {
        this.montodte = montodte;
    }

    public String getDvconsultante() {
        return dvconsultante;
    }

    public void setDvconsultante(String dvconsultante) {
        this.dvconsultante = dvconsultante;
    }

    

  
}
