package com.egga.appdtews.appdte.rest.resources;

import com.egga.appdtews.appdte.json.DteJson;
import com.egga.appdtews.appdte.json.TrackID;
import com.egga.appdtews.service.DteService;
import com.egga.appdtews.service.ServiceFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@WebServlet("/sendDTE")
public class SendDteResource extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(SendDteResource.class);
    private final DteService dteService = ServiceFactory.createDteService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        try {
            // Leer el cuerpo de la solicitud
            String requestBody = new String(req.getInputStream().readAllBytes(), StandardCharsets.UTF_8);

            // Mapear el JSON al objeto DteJson
            ObjectMapper objectMapper = new ObjectMapper();
            DteJson objDTE = objectMapper.readValue(requestBody, DteJson.class);

            // Procesar el DTE
            logger.info("Procesando DTE");
            String response = dteService.processDte(objDTE);

            // Responder
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.getWriter().write(response);

        } catch (Exception e) {
            logger.error("Error al procesar DTE", e);
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            resp.getWriter().write(handleError(e));
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");

        try {
            // Cargar el archivo HTML
            InputStream htmlStream = getClass().getResourceAsStream("/docs/enviodte.html");
            if (htmlStream == null) {
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                resp.getWriter().write("Documentation not found.");
                return;
            }

            // Enviar el contenido del HTML
            String htmlContent = new String(htmlStream.readAllBytes(), StandardCharsets.UTF_8);
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.getWriter().write(htmlContent);

        } catch (Exception e) {
            logger.error("Error al cargar la documentación", e);
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            resp.getWriter().write("Error loading documentation.");
        }
    }

    private String handleError(Exception e) throws JsonProcessingException {
        TrackID error = new TrackID();
        error.setEstado("ERROR");
        error.setMensajerror(e.getMessage());

        // Convertir el objeto TrackID a JSON
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(error);
    }
}
