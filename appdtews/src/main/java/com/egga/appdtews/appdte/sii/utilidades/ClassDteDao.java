/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
package com.egga.appdtews.appdte.sii.utilidades;
import com.egga.appdtews.appdte.json.DescGlobalJson;
import com.egga.appdtews.appdte.json.DetalleDteJson;
import com.egga.appdtews.appdte.json.DteJson;
import com.egga.appdtews.appdte.json.EmisorJson;
import com.egga.appdtews.appdte.json.IdDteJson;
import com.egga.appdtews.appdte.json.ImptoRetenJson;
import com.egga.appdtews.appdte.json.ReceptorJson;
import com.egga.appdtews.appdte.json.ReferenciaJson;
import com.egga.appdtews.appdte.json.TotalesJson;
import com.egga.appdtews.appdte.json.VlrCodigoJson;
import com.egga.appdtews.utils.funciones.FunctionChar;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import javax.xml.transform.OutputKeys;

public class ClassDteDao {
    
    private Document doc;
    private Element documento;

public void crearXml(DteJson objDteJson) throws TransformerConfigurationException, TransformerException, ParserConfigurationException, SAXException, IOException{
    	
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		this.doc = docBuilder.newDocument();
		
                Element dte = this.doc.createElement("DTE");
                Attr attrversion = this.doc.createAttribute("version");
	        attrversion.setValue("1.0"); 
                dte.setAttributeNode(attrversion);
                this.doc.appendChild(dte);
                this.documento = this.doc.createElement("Documento");
                documento.setAttribute("ID", "DOC33");
             
                dte.appendChild(this.documento);
                
                
                IdDteJson objIdDte = objDteJson.getIddoc();
                
                
                  //inicio de encabezado de documento
		Element encabezado = this.doc.createElement("Encabezado");
		this.documento.appendChild(encabezado);
               
                Element iddoc = this.doc.createElement("IdDoc");
                encabezado.appendChild(iddoc);
               
                Element tipodte = this.doc.createElement("TipoDTE");
                tipodte.setTextContent(objIdDte.getTipodte());
                
               iddoc.appendChild(tipodte);
                
                Element folio = this.doc.createElement("Folio");
                folio.setTextContent(objIdDte.getFolio());
                iddoc.appendChild(folio);

                Element fechaemis = this.doc.createElement("FchEmis");
                fechaemis.setTextContent(objIdDte.getFchemis());
                iddoc.appendChild(fechaemis);
                
                if(Integer.parseInt(tipodte.getTextContent())==52){
                     Element tipotraslado = this.doc.createElement("IndTraslado");
                     tipotraslado.setTextContent(objIdDte.getIndtraslado());
                     iddoc.appendChild(tipotraslado);
                }
                
                
                 if( Integer.parseInt(tipodte.getTextContent())==33 || Integer.parseInt(tipodte.getTextContent())==34    ){
                     Element frmapago = this.doc.createElement("FmaPago");
                     frmapago.setTextContent(objIdDte.getFmapago());
                     iddoc.appendChild(frmapago);
                }
                
                 
                 
                if(objIdDte.getFchvenc()!=null){
                      Element fchvenc = this.doc.createElement("FchVenc");
                     fchvenc.setTextContent(objIdDte.getFchvenc());
                     iddoc.appendChild(fchvenc);
                } 
                 
                 
                
                // agrego los datos del emisor de la fctura
                                               
                Element emisor = this.doc.createElement("Emisor");
                encabezado.appendChild(emisor);
                
                
                EmisorJson objEmisor = objDteJson.getEmisor();
                
                Element rutemisor = this.doc.createElement("RUTEmisor");
                rutemisor.setTextContent(objEmisor.getRutemisor());
                emisor.appendChild(rutemisor);
                
                
                
                 FunctionChar objFormato = new FunctionChar();    
      
      
                
                
                
                Element razonsocial = this.doc.createElement("RznSoc");
                razonsocial.setTextContent(objFormato.formatoTexto(objEmisor.getRznsoc()));
                emisor.appendChild(razonsocial);
                 
                   Element giroemisor = this.doc.createElement("GiroEmis");
                   giroemisor.setTextContent(objFormato.formatoTexto(objEmisor.getGiroemis()));
                   emisor.appendChild(giroemisor);
                   
                   
                   Element acteco = this.doc.createElement("Acteco");
                   acteco.setTextContent(objEmisor.getActeco());
                   emisor.appendChild(acteco);
                    
                   Element cdgsiisucur = this.doc.createElement("CdgSIISucur");
                   cdgsiisucur.setTextContent(objEmisor.getCdgsiisucur());
                   emisor.appendChild(cdgsiisucur);
                   
                   
                   Element dirorigen = this.doc.createElement("DirOrigen");
                   dirorigen.setTextContent(objFormato.formatoTexto(objEmisor.getDirorigen()));
                   emisor.appendChild(dirorigen);
                   
                   
                   Element cmnaorigen = this.doc.createElement("CmnaOrigen");
                   cmnaorigen.setTextContent(objFormato.formatoTexto(objEmisor.getCmnaorigen()));
                   emisor.appendChild(cmnaorigen);
                   
                   
                   Element ciudadorigen = this.doc.createElement("CiudadOrigen");
                   ciudadorigen.setTextContent(objFormato.formatoTexto(objEmisor.getCiudadorigen()));
                   emisor.appendChild(ciudadorigen);
              
                   
                   // ahora agrego los datos del receptor
                   
                   ReceptorJson objReceptor = objDteJson.getReceptor();
                   
                   
                   Element receptor = this.doc.createElement("Receptor");
                   encabezado.appendChild(receptor);
                   
                   Element rutrecep = this.doc.createElement("RUTRecep");
                   rutrecep.setTextContent(objReceptor.getRutrecep());
                   receptor.appendChild(rutrecep);
                   
                   Element rznsocrecep = this.doc.createElement("RznSocRecep");
                   rznsocrecep.setTextContent(objFormato.formatoTexto(objReceptor.getRznsocrecep()));
                   receptor.appendChild(rznsocrecep);
                   
                   Element girorecep = this.doc.createElement("GiroRecep");
                   girorecep.setTextContent(objReceptor.getGirorecep());
                   receptor.appendChild(girorecep);
                   
                   Element dirrecep = this.doc.createElement("DirRecep");
                   dirrecep.setTextContent(objFormato.formatoTexto(objReceptor.getDirrecep()));
                   receptor.appendChild(dirrecep);
                   
                   
                   Element cmnarecep = this.doc.createElement("CmnaRecep");
                   cmnarecep.setTextContent(objFormato.formatoTexto(objReceptor.getCmnarecep()));
                   
                   receptor.appendChild(cmnarecep);
                   
                   Element ciudadrecep = this.doc.createElement("CiudadRecep");
                   ciudadrecep.setTextContent(objFormato.formatoTexto(objReceptor.getCiudadrecep()));
                   receptor.appendChild(ciudadrecep);
                         
              /* EN ESTA ZONA AGREGO LOS TOTALES DEL DOCUMENTO */     
             Element totales;
             Element mntneto;
             Element tasaiva;
             Element iva;
             Element mnttotal;
             Element mntexe; /* monto exento */
             Element montonf;
             Element montoperiodo;    
             
             
             TotalesJson objTotales = objDteJson.getTotales();
             
             
            totales = this.doc.createElement("Totales");
            encabezado.appendChild(totales);
             try{
                if(objTotales.getMntneto()!=null){ 
                 
            mntneto = this.doc.createElement("MntNeto");
            mntneto.setTextContent(objTotales.getMntneto());
            totales.appendChild(mntneto);
                }
            
            }catch(NullPointerException e) {
                
              
                      System.out.println("Se detectó una NullPointerException: " + e.getMessage());
   
            }
           
           
            try{
                
                
                if(objTotales.getMntexe()!=null){
                mntexe = this.doc.createElement("MntExe");
                mntexe.setTextContent(objTotales.getMntexe());
                totales.appendChild(mntexe);
                }
            }catch(NullPointerException e){
                
                
                      System.out.println("Se detectó una NullPointerException: " + e.getMessage());
   
            }
            
           try{
              
            if(objTotales.getTasaiva()!=null){
             
            tasaiva = this.doc.createElement("TasaIVA");
            tasaiva.setTextContent(objTotales.getTasaiva());
            totales.appendChild(tasaiva);
                }
           }
           
           catch(NullPointerException e){
                
                
                    System.out.println("Se detectó una NullPointerException: " + e.getMessage());
   
            }
           
           
           
            try{
            
            if(objTotales.getIva()!=null){
                
            iva = this.doc.createElement("IVA");
            iva.setTextContent(objTotales.getIva());
            totales.appendChild(iva);
            }
            }catch(NullPointerException e){
                
                  System.out.println("Se detectó una NullPointerException: " + e.getMessage());
   
                
            }
            
            
            
            /* antes del monto total coloco los imptos retenidos */
            List<ImptoRetenJson> array_impuestos = objTotales.getImptoreten();
            
            
            if(array_impuestos!=null){ 
 
                for (ImptoRetenJson x :  array_impuestos){
    
                Element ImptoReten = this.doc.createElement("ImptoReten");
                Element TipoImp = this.doc.createElement("TipoImp");
                Element TasaImp = this.doc.createElement("TasaImp");
                Element MontoImp = this.doc.createElement("MontoImp");

                TipoImp.setTextContent(x.getTipoimp());
                TasaImp.setTextContent(x.getTasapimp());
                MontoImp.setTextContent(x.getMontoimp());
      
                ImptoReten.appendChild(TipoImp);
                ImptoReten.appendChild(TasaImp);
                ImptoReten.appendChild(MontoImp);
    
               totales.appendChild(ImptoReten);
   
             }     
           }      
            
            
            
              try{
            
            
            mnttotal = this.doc.createElement("MntTotal");
            mnttotal.setTextContent(objTotales.getMnttotal());
            totales.appendChild(mnttotal);
      
              }catch(NullPointerException e ){
                  
                      System.out.println("Se detectó una NullPointerException: " + e.getMessage());
   
                  
              }
            
            
            
            try{
            
            if(objTotales.getMontonf()!=null){    
            montonf = this.doc.createElement("MontoNF");
            montonf.setTextContent(objTotales.getMontonf());
            totales.appendChild(montonf);
            }
            } catch(NullPointerException e) {
                 System.out.println("Se detectó una NullPointerException: " + e.getMessage());
   
                
            }
            
            
            try{
            
            if(objTotales.getMntperiodo()!=null){
            montoperiodo = this.doc.createElement("MontoPeriodo");
            montoperiodo.setTextContent(objTotales.getMntperiodo());
            totales.appendChild(montoperiodo);
            }
            }catch(NullPointerException e) {
                 System.out.println("Se detectó una NullPointerException: " + e.getMessage());   
            }
            
            
} 
                






public void agregaDetalle(DetalleDteJson detalledte){
        
            // agrega detalles al documentos xml //
             Element detalle;
             Element nrolindet;
             Element cdgitem;
             Element tpocodigo;
             Element vlrcodigo;
             Element nmbitem;
             Element dscitem;
             Element qtyitem;
             Element prcitem;
             Element montoitem;
             Element indexe;
             
             /* CodImpAdic */
             
             Element codimpadic;
           FunctionChar objFormato = new FunctionChar();    
      
      
             
             
             
             
             detalle = this.doc.createElement("Detalle");
    
             nrolindet = this.doc.createElement("NroLinDet");
             nrolindet.setTextContent(Integer.toString(detalledte.getNrolindet()));
             detalle.appendChild(nrolindet);
      
            for(VlrCodigoJson i: detalledte.getCdgitem()){ 
             
             cdgitem = this.doc.createElement("CdgItem");
             detalle.appendChild(cdgitem);
      
             tpocodigo = this.doc.createElement("TpoCodigo");
             tpocodigo.setTextContent(i.getTpocodigo());
             cdgitem.appendChild(tpocodigo);
             
             vlrcodigo = this.doc.createElement("VlrCodigo");
             vlrcodigo.setTextContent(objFormato.formatoTexto(i.getVlrcodigo()));
             cdgitem.appendChild(vlrcodigo);
            }
             
             
            if(!"0".equals(detalledte.getIndexe())){
                
                indexe = this.doc.createElement("IndExe");
                indexe.setTextContent(String.valueOf(detalledte.getIndexe()));
                detalle.appendChild(indexe);
            }
             
             
             
             nmbitem = this.doc.createElement("NmbItem");
             nmbitem.setTextContent(objFormato.formatoTexto(detalledte.getNmbitem().trim()));
             detalle.appendChild(nmbitem);
          
             /*
             dscitem = this.doc.createElement("DscItem");
          
             dscitem.setTextContent(objFormato.formatoTexto(detalledte..trim()));
             detalle.appendChild(dscitem);
            */
             
             qtyitem = this.doc.createElement("QtyItem");
             qtyitem.setTextContent(detalledte.getQtyitem());
             detalle.appendChild(qtyitem);
          
             prcitem = this.doc.createElement("PrcItem");
             prcitem.setTextContent(detalledte.getPrcitem());
             detalle.appendChild(prcitem);
             
          
             if(!"0".equals(detalledte.getDescuentopct())){
             
                Element descuentopct = this.doc.createElement("DescuentoPct");
                Element descuentomonto = this.doc.createElement("DescuentoMonto");
                
                descuentopct.setTextContent(detalledte.getDescuentopct());
                detalle.appendChild(descuentopct);
                
                descuentomonto.setTextContent(detalledte.getDescuentomonto());
                detalle.appendChild(descuentomonto);
             }         
                         
             /* CodImpAdic */
            if(detalledte.getCodimpadic()!=null){ 
             codimpadic = this.doc.createElement("CodImpAdic");
             codimpadic.setTextContent(detalledte.getCodimpadic());
             detalle.appendChild(codimpadic);
            }
             montoitem = this.doc.createElement("MontoItem");
             montoitem.setTextContent(detalledte.getMontoitem());
             detalle.appendChild(montoitem);

             this.documento.appendChild(detalle);
         
}        
          
       public void agregaRegerencia(ReferenciaJson referencia){
               
           FunctionChar objFormato = new FunctionChar();    

           Element nodoref = this.doc.createElement("Referencia");
           Element NroLinRef = this.doc.createElement("NroLinRef");
           NroLinRef.setTextContent(String.valueOf(referencia.getNrolinref()));
           nodoref.appendChild(NroLinRef);
         
          
        
           Element TpoDocRef = this.doc.createElement("TpoDocRef"); 
           TpoDocRef.setTextContent(String.valueOf(referencia.getTpodocref()));
           nodoref.appendChild(TpoDocRef);
           
         
           Element FolioRef = this.doc.createElement("FolioRef");
           FolioRef.setTextContent(String.valueOf(referencia.getFolioref()));
           nodoref.appendChild(FolioRef);
            
           Element FchRef = this.doc.createElement("FchRef");
           FchRef.setTextContent(referencia.getFchref());
           nodoref.appendChild(FchRef);
       
           if(!"0".equals(referencia.getCodref())){
           Element CodRef = this.doc.createElement("CodRef");
               CodRef.setTextContent(referencia.getCodref());
               nodoref.appendChild(CodRef);
           }
           Element RazonRef = this.doc.createElement("RazonRef");
           RazonRef.setTextContent(objFormato.formatoTexto(referencia.getRazonref()));
           nodoref.appendChild(RazonRef);
           this.documento.appendChild(nodoref);
    } 
        
      
    
    
    public void guardarDocumento(String nombredte,String pathdte) throws TransformerException, ParserConfigurationException, SAXException, IOException{
        
        
           //esta seccion se encarga de hacer la identacion
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        Transformer transformer = transformerFactory.newTransformer();     
              
                transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
              
                transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "0"); 
                            //luego guardo el documento    
                DOMSource source = new DOMSource(this.doc);              
                StreamResult result = new StreamResult(new File(pathdte+  nombredte+".xml"));
		transformer.transform(source, result);                
		
        
   }
    
    public void agregaDescuento(DescGlobalJson obj){
    /*
    
<DscRcgGlobal>
<NroLinDR>1</NroLinDR>
<TpoMov>D</TpoMov>
<GlosaDR>Porcentaje Variable</GlosaDR>
<TpoValor>%</TpoValor>
<ValorDR>13</ValorDR>
</DscRcgGlobal>
    */
    
    Element DscRcgGlobal = this.doc.createElement("DscRcgGlobal");
    Element NroLinDR = this.doc.createElement("NroLinDR");
    Element TpoMov = this.doc.createElement("TpoMov");
  
    
    
    NroLinDR.setTextContent(obj.getNrolindr());
    TpoMov.setTextContent(obj.getTpoMov());
    Element GlosaDR = this.doc.createElement("GlosaDR");
    GlosaDR.setTextContent(obj.getGlosadr());
    Element TpoValor = this.doc.createElement("TpoValor");
    TpoValor.setTextContent(obj.getTpovalor());
    Element ValorDR = this.doc.createElement("ValorDR");
    ValorDR.setTextContent(obj.getValordr());
    
    
 
    
    DscRcgGlobal.appendChild(NroLinDR);
    DscRcgGlobal.appendChild(TpoMov);
    DscRcgGlobal.appendChild(GlosaDR);
    DscRcgGlobal.appendChild(TpoValor);
    DscRcgGlobal.appendChild(ValorDR);
    if(!"0".equals(obj.getIndexedr()) && obj.getIndexedr()!=null){
           Element IndExeDR = this.doc.createElement("IndExeDR");
        DscRcgGlobal.appendChild(IndExeDR);
    }
    
    
    
  
    
    this.documento.appendChild(DscRcgGlobal);
    
    
}       
    
    
    
    
    
            
}

    
    
    
    
    
    
    
    






