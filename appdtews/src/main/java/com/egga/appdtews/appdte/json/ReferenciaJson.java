/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.appdte.json;

/**
 *
 * @author esteban
 */
public class ReferenciaJson {
    
   
  private String codref="0";
  private String razonref;
  private String nrolinref; 
  private String tpodocref;
  private String folioref;
  private String fchref;

    
   
   
    public String getRazonref() {
        return razonref;
    }

    public void setRazonref(String razonref) {
        this.razonref = razonref;
    }

    public String getNrolinref() {
        return nrolinref;
    }

    public void setNrolinref(String nrolinref) {
        this.nrolinref = nrolinref;
    }


    public String getFolioref() {
        return folioref;
    }

    public void setFolioref(String folioref) {
        this.folioref = folioref;
    }

    public String getFchref() {
        return fchref;
    }

    public void setFchref(String fchref) {
        this.fchref = fchref;
    }

    public String getCodref() {
        return codref;
    }

    public void setCodref(String codref) {
        this.codref = codref;
    }

    public String getTpodocref() {
        return tpodocref;
    }

    public void setTpodocref(String tpodocref) {
        this.tpodocref = tpodocref;
    }

    
    
    
}
