/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.appdte.json;

/**
 *
 * @author esteban
 */
public class ImptoRetenJson {
 private String tipoimp;
 private String tasapimp;
 private String montoimp;

    public String getTipoimp() {
        return tipoimp;
    }

    public void setTipoimp(String tipoimp) {
        this.tipoimp = tipoimp;
    }

    public String getTasapimp() {
        return tasapimp;
    }

    public void setTasapimp(String tasapimp) {
        this.tasapimp = tasapimp;
    }

    public String getMontoimp() {
        return montoimp;
    }

    public void setMontoimp(String montoimp) {
        this.montoimp = montoimp;
    }
         
}
