/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
package com.egga.appdtews.appdte.sii.utilidades;

import com.egga.appdtews.documento.DocumentoModel;
import com.egga.appdtews.empresa.EmpresaModel;
import com.egga.appdtews.movimientos.Movimiento;
import com.egga.appdtews.movimientos.MovimientoModel;
import com.egga.appdtews.appdte.json.DescGlobalJson;
import com.egga.appdtews.appdte.json.ReceptorJson;
import com.egga.appdtews.appdte.json.EmisorJson;
import com.egga.appdtews.appdte.json.IdDteJson;
import com.egga.appdtews.appdte.json.ReferenciaJson;
import com.egga.appdtews.appdte.json.DetalleDteJson;
import com.egga.appdtews.appdte.json.TotalesJson;
import com.egga.appdtews.appdte.json.DteJson;
import com.egga.appdtews.appdte.json.TrackID;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;
import com.egga.appdtews.appdte.sii.cl.Semilla;
import com.egga.appdtews.appdte.sii.cl.Token;
import com.egga.appdtews.appdte.sii.cl.UploadSii;
import com.fasterxml.jackson.databind.ObjectMapper;
/*
import appventas.movimientos.BlobDTE;

*/
import java.util.List;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;


public class AppDTE {
    String environment;
    
            
            
    
    
    
    @SuppressWarnings("empty-statement")
    
    public String sendDTE(String stringDTE,String certificado,String clave,String rutEnvia, boolean blReferencia) throws TransformerException, ParserConfigurationException, SAXException, IOException, Exception{

        
        
        
        
        String login = certificado;
        
        
        
   ConfigAppDTE objconfig = new ConfigAppDTE();
        /* CARGO LOS PARAMETROS DE CONFIGURACION */
        /* ingreso el DTE en formato JSON */
  
             
   System.out.println("Reading JSON from a file");
   System.out.println("----------------------------");
  
   
  
    InputStream isjson = new ByteArrayInputStream(stringDTE.getBytes("ISO-8859-1")); 
    BufferedReader br1 = new BufferedReader(new InputStreamReader(isjson));
  
  
 
    ObjectMapper objectMapperDTE = new ObjectMapper();
    DteJson objdtejson = objectMapperDTE.readValue(stringDTE,DteJson.class); 
    
     
     /* DATOS DEL EMISOR EN JSON */
     EmisorJson objemisor = objdtejson.getEmisor();
     
     
     
     
     
    /* DATOS DEL RECEPTOR EN JSON */
    ReceptorJson objreceptor = objdtejson.getReceptor();
    
    IdDteJson iddoc = objdtejson.getIddoc();
    
    
    
    
    
    
   /* inicializar el xml */        
  
    
    ClassDteDao obj = new ClassDteDao();
    /* DEFINO DATOS DEL EMISOR Y RECEPTOR 
   */
   
    
    /* VALIDAMOS CAF */
        /*
        if(objFuncionCAF.validaCAF(objconfig.getPathcaf(), objemisor.getRutemisor(),Integer.parseInt(iddoc.getTipoDTE()), Integer.parseInt(iddoc.getNumDTE()))==false){
         */
        /*
        return null;
         */
        /*  }*/
    
    
    
    EmisorJson objEmisor = objdtejson.getEmisor();
        
    
    String[] arrayrutemisor = objEmisor.getRutemisor().split("-");
    
    String rutemisor = arrayrutemisor[0];
    String nombredte = "DTE"+rutemisor+"F"+iddoc.getFolio()+"T"+iddoc.getTipodte();
        
    
    
    
    
    /* DATOS DEL EMISOR EN EL XML */
  
    
    obj.crearXml(objdtejson);
    
      /* cargo los detalles */
     List<DetalleDteJson> detalle = objdtejson.getDetalle();
  
    
for (DetalleDteJson i :  detalle){     
        
    obj.agregaDetalle(i);
   
    }

/* ADJUNTO LOS DESCUENTOS GLOBALES   */

List<DescGlobalJson> arraydescuentos = objdtejson.getDscrcgglobal();

if(arraydescuentos!=null){ 
for (DescGlobalJson x :  arraydescuentos){
    
   obj.agregaDescuento(x);
   
   
}     
}      



 try{
   ReferenciaJson referencia = objdtejson.getReferencia();
 
    
obj.agregaRegerencia(referencia);
    

  }catch(Exception e){
 System.out.print("IGNORA REFERENCIA");     
      
  }
  


obj.guardarDocumento(nombredte,objconfig.getPathdte());



/*
objTimbre.creaTimbre(objdte, auxDescripcion,rutemisor);
  
    
/* preparo el DTE para firmar */

/* preparo el DTE para firmar */

Timbre objTimbre = new Timbre();
 
objTimbre.creaTimbre(login,objconfig.getPathdte(),nombredte,objconfig.getPathdata(),objconfig.getPathcaf(),rutemisor);



SignDTE objFirma = new SignDTE();
objFirma.signDTE(objconfig.getPathdte(),nombredte,certificado,clave);
   
    /* ahora envuelvo el DTE en un sobre electrónico */
   
EnvioDTE objenvio = new EnvioDTE(this.environment, objreceptor.getRutcaratula());
objenvio.generaEnvio(objdtejson,nombredte,objconfig.getPathdte(),rutEnvia);

SignENVDTE objFirmaENV = new SignENVDTE();
objFirmaENV.signENVDTE(objconfig.getPathdte(),nombredte,certificado,clave);
    

 /* OBTENGO LA SEMILLA PARA AUTENTIFICARME AL SII   */ 
 
  Semilla objsemilla = new Semilla();

  
 
String valorsemilla =  objsemilla.getSeed(objconfig.getPathenvironment());
 

 Token objtoken = new Token(objconfig.getPathenvironment());
 String valortoken =  objtoken.getToken(valorsemilla,certificado,clave,nombredte);


UploadSii objupload = new UploadSii(objconfig.getPathenvironment());
 





TrackID objTrackID = objupload.uploadSii(valortoken,"","ENV"+ nombredte,objEmisor.getRutemisor(),rutEnvia);

   
   EmpresaModel objEmpresaModel = new EmpresaModel();  
   int empresaid = objEmpresaModel.getIdEmpresafromRut(objemisor.getRutemisor());
   DocumentoModel objDocumentoModel = new DocumentoModel();
   
   int idtipodoc = objDocumentoModel.getIddocumento2(Integer.parseInt(iddoc.getTipodte()));
   Movimiento objMovimiento = new Movimiento();
   TotalesJson objtotales = objdtejson.getTotales();
   objMovimiento.setFecha(iddoc.getFchemis());
   objMovimiento.setEmpresaid(empresaid);
   objMovimiento.setRutemisor(objemisor.getRutemisor());
   objMovimiento.setRutreceptor(objreceptor.getRutcaratula());
   objMovimiento.setRutenvia(rutEnvia);
   objMovimiento.setIdtipodoc(idtipodoc);
   objMovimiento.setTotalbruto(objtotales.getMnttotal());
   
   
   objMovimiento.setMontoexento(objtotales.getMntexe());
   objMovimiento.setMontoiva(objtotales.getIva());
   objMovimiento.setMontoneto(objtotales.getMntneto());
   objMovimiento.setMontonofacturado(objtotales.getMontonf());
   objMovimiento.setMontoperiodo(objtotales.getMntperiodo());
   objMovimiento.setTipoOperacion(objdtejson.getTipoOperacion());
   
   
   
   
   objMovimiento.setNumdoc(Integer.parseInt(iddoc.getFolio()));
   objMovimiento.setArchivonom(objTrackID.getFile());
   objMovimiento.setTrackid(objTrackID.getTrackid());
   MovimientoModel objMovimientoModel = new MovimientoModel();
   objMovimientoModel.addDTE(objMovimiento);
   
   
   ObjectMapper oBJmapper = new ObjectMapper();
   StringWriter stringTrackid = new StringWriter();
   oBJmapper.writeValue(stringTrackid, objTrackID);
 
   return stringTrackid.toString();
 
 
 
}
   

}