/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.appdte.json;

import java.util.List;

public class TotalesJson {
    
    
   private String mntneto;
   private String mntexe;
   private String iva;
   private String mnttotal;
   private String tasaiva;
   private String montonf;
   private String mntperiodo;
   
   
   
 public TotalesJson(){
    /* 
   this.MntNeto = 0;
   this.MntExe = 0;
   this.IVA = 0;
   this.MntTotal = 0;
   this.TasaIVA = 0;
   this.montoneto = 0;
   this.MontoNF = 0;
   this.MontoPeriodo = 0;
     */
     
 }  
   
   
    private List<ImptoRetenJson> imptoreten = null;
    
 

    

   

    public List<ImptoRetenJson> getImptoreten() {
        return imptoreten;
    }

    public void setImptoreten(List<ImptoRetenJson> imptoreten) {
        this.imptoreten = imptoreten;
    }

    public String getMntneto() {
        return mntneto;
    }

    public void setMntneto(String mntneto) {
        this.mntneto = mntneto;
    }

    public String getMntexe() {
        return mntexe;
    }

    public void setMntexe(String mntexe) {
        this.mntexe = mntexe;
    }

    public String getIva() {
        return iva;
    }

    public void setIva(String iva) {
        this.iva = iva;
    }

    public String getMnttotal() {
        return mnttotal;
    }

    public void setMnttotal(String mnttotal) {
        this.mnttotal = mnttotal;
    }

    public String getTasaiva() {
        return tasaiva;
    }

    public void setTasaiva(String tasaiva) {
        this.tasaiva = tasaiva;
    }

    public String getMontonf() {
        return montonf;
    }

    public void setMontonf(String montonf) {
        this.montonf = montonf;
    }

    public String getMntperiodo() {
        return mntperiodo;
    }

    public void setMntperiodo(String mntperiodo) {
        this.mntperiodo = mntperiodo;
    }

   
  
}


