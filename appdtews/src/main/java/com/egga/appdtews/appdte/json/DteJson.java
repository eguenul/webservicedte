/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.appdte.json;

import java.util.List;

/**
 *
 * @author esteban
 */
public class DteJson {
       
    private EmisorJson emisor;
    private ReceptorJson receptor;
    private IdDteJson iddoc;
    private List<DetalleDteJson> detalle;
    private TotalesJson totales;
    private ReferenciaJson referencia; 
    private List <DescGlobalJson> dscrcgglobal;
    private UsuarioJson usuario; 
    private String tipoOperacion; 


    public DteJson(){


    }

    public EmisorJson getEmisor() {
        return emisor;
    }

    public void setEmisor(EmisorJson emisor) {
        this.emisor = emisor;
    }

    public ReceptorJson getReceptor() {
        return receptor;
    }

    public void setReceptor(ReceptorJson receptor) {
        this.receptor = receptor;
    }

    public IdDteJson getIddoc() {
        return iddoc;
    }

    public void setIddoc(IdDteJson iddoc) {
        this.iddoc = iddoc;
    }

    public List<DetalleDteJson> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<DetalleDteJson> detalle) {
        this.detalle = detalle;
    }

    public TotalesJson getTotales() {
        return totales;
    }

    public void setTotales(TotalesJson totales) {
        this.totales = totales;
    }

    public ReferenciaJson getReferencia() {
        return referencia;
    }

    public void setReferencia(ReferenciaJson referencia) {
        this.referencia = referencia;
    }

    public List <DescGlobalJson> getDscrcgglobal() {
        return dscrcgglobal;
    }

    public void setDscrcgglobal(List <DescGlobalJson> dscrcgglobal) {
        this.dscrcgglobal = dscrcgglobal;
    }

    public UsuarioJson getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioJson usuario) {
        this.usuario = usuario;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }



}