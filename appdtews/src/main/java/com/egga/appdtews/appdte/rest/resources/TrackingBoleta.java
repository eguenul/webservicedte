package com.egga.appdtews.appdte.rest.resources;

import com.egga.appdtews.appdte.json.TrackingJSON;
import com.egga.appdtews.service.ServiceFactory;
import com.egga.appdtews.service.TrackingBoletaService;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import jakarta.ws.rs.core.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/trackingboleta")
public class TrackingBoleta extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(TrackingBoleta.class.getName());
    private final TrackingBoletaService trackingBoletaService = ServiceFactory.createTrackingBoletaService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // Leer el cuerpo de la solicitud (JSON)
            InputStream inputStream = request.getInputStream();
            String json = new String(inputStream.readAllBytes());
            
            // Aquí debes convertir el JSON a un objeto TrackingJSON
            // Esto lo puedes hacer usando una librería como Jackson o Gson
            TrackingJSON objTrack = new ObjectMapper().readValue(json, TrackingJSON.class);

            String result = trackingBoletaService.trackBoleta(objTrack);

            response.setContentType(MediaType.APPLICATION_JSON);
            response.getWriter().write(result);
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "Error while tracking boleta", ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("{\"error\": \"Error processing boleta\"}");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            InputStream htmlStream = getClass().getResourceAsStream("/docs/boletastate.html");
            if (htmlStream == null) {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                response.getWriter().write("Documentation not found.");
            } else {
                response.setContentType(MediaType.TEXT_HTML);
                response.getOutputStream().write(htmlStream.readAllBytes());
                response.setStatus(HttpServletResponse.SC_OK);
            }
        } catch (IOException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("Error loading documentation.");
        }
    }
}
