/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
package com.egga.appdtews;

import com.egga.appdtews.admincaf.AdminCaf;
import com.egga.appdtews.admincaf.CafOk;
import com.egga.appdtews.admincaf.DeleteCaf;
import com.egga.appdtews.admincaf.ErrorCaf;
import com.egga.appdtews.admincaf.UploadCaf;
import com.egga.appdtews.appdte.rest.resources.APIDocs;
import com.egga.appdtews.appdte.rest.resources.License;
import com.egga.appdtews.appdte.rest.resources.SendDteResource;
import com.egga.appdtews.appdte.rest.resources.TrackingBoleta;
import com.egga.appdtews.appdte.rest.resources.getDTE;
import com.egga.appdtews.appdte.rest.resources.getTEDWS;
import com.egga.appdtews.appdte.rest.resources.printDTE;
import com.egga.appdtews.empresa.AddEmpresa;
import com.egga.appdtews.empresa.EmpresaOk;
import com.egga.appdtews.empresa.EmpresaServlet;
import com.egga.appdtews.empresa.EmpresaServlet2;
import com.egga.appdtews.empresa.ListEmpresa;
import com.egga.appdtews.empresa.SeleccionEmpresa;
import com.egga.appdtews.filters.AdminFilter;
import com.egga.appdtews.filters.AuthFilter;
import com.egga.appdtews.login.AdminLogin;
import com.egga.appdtews.login.ErrorLogin;

import com.egga.appdtews.login.LoginServlet;
import com.egga.appdtews.login.Logout;
import com.egga.appdtews.login.SetPassServlet;
import com.egga.appdtews.usuarios.AddUsuario;
import com.egga.appdtews.usuarios.AdminCert;
import com.egga.appdtews.usuarios.BuscarUsuario;
import com.egga.appdtews.usuarios.BusquedaApellido;
import com.egga.appdtews.usuarios.BusquedaLogin;
import com.egga.appdtews.usuarios.BusquedaNombre;
import com.egga.appdtews.usuarios.ListadoUsuario;
import com.egga.appdtews.usuarios.SetFirmaPass;
import com.egga.appdtews.usuarios.UploadCert;
import com.egga.appdtews.usuarios.UsuarioServlet;
import com.egga.appdtews.web.IndexServlet;
import com.egga.appdtews.web.RootRedirectServlet;
import jakarta.servlet.MultipartConfigElement;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.Resource;

public class AppDTEWS {
    
        public static void main(String[] args) throws Exception {
           
        
       // Crear una instancia del servidor en el puerto 8080
        Server server = new Server(8080);
      // Configurar el contexto web para JSP y recursos REST
     
    WebAppContext context = new WebAppContext();
    context.setContextPath("/AppDTEWS");
    context.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "true");  
    context.setResourceBase("src/main/resources/static"); // Ruta de los recursos estáticos

    
    
ResourceHandler jarResources = new ResourceHandler();
jarResources.setBaseResource(Resource.newClassPathResource("/static"));

context.addServlet(DefaultServlet.class, "/css/*");
context.addServlet(DefaultServlet.class, "/scripts/*");
context.addServlet(DefaultServlet.class, "/images/*");
context.addServlet(DefaultServlet.class, "/messageview/*");
            
    
       
       
        FilterHolder authFilter = new FilterHolder(new AuthFilter());
        context.addFilter(authFilter, "/secured/*", null);
        
      FilterHolder cafFilter = new FilterHolder(new AdminFilter());
      context.addFilter(cafFilter, "/secured/adminCAF", null);
        
      
      
      
      
      
      FilterHolder usuarioFilter = new FilterHolder(new AdminFilter());
      context.addFilter(usuarioFilter, "/secured/usuario", null);
        
         FilterHolder empresaFilter = new FilterHolder(new AdminFilter());
      context.addFilter(empresaFilter, "/secured/empresa", null);
        
        
          FilterHolder adminpassFilter = new FilterHolder(new AdminFilter());
      context.addFilter(adminpassFilter, "/secured/setpass", null);
        
        
      
          FilterHolder listempresaFilter = new FilterHolder(new AdminFilter());
      context.addFilter(listempresaFilter, "/secured/listempresa", null);
        
        
        
        
         // Registrar un servlet adicional
        ServletHolder loginServlet = new ServletHolder(new LoginServlet());
        context.addServlet(loginServlet, "/login");
        
        
        ServletHolder logout = new ServletHolder(new Logout());
        context.addServlet(logout, "/logout");
        
        
        
        
        
         // Registrar un servlet adicional
        ServletHolder errorlogin = new ServletHolder(new ErrorLogin());
        context.addServlet(errorlogin, "/errorlogin");
        
        
         // Registrar un servlet adicional
        ServletHolder adminlogin = new ServletHolder(new AdminLogin());
        context.addServlet(adminlogin, "/adminlogin");
        
        
        
        
        
        

        ServletHolder empresaServlet = new ServletHolder(new EmpresaServlet());
        context.addServlet(empresaServlet, "/secured/empresa");
 
        
        ServletHolder addempresa = new ServletHolder(new AddEmpresa());
        context.addServlet(addempresa, "/secured/addempresa");
 
       
         
        ServletHolder empresaok = new ServletHolder(new EmpresaOk());
        context.addServlet(empresaok, "/secured/empresaok");
 
        ServletHolder empresaerror = new ServletHolder(new EmpresaOk());
        context.addServlet(empresaerror, "/secured/empresaerror");
 
        
        
        
        
        
        
        ServletHolder selempresaServlet = new ServletHolder(new SeleccionEmpresa());
        context.addServlet(selempresaServlet, "/secured/selempresa");
               
        ServletHolder empresaServlet2 = new ServletHolder(new EmpresaServlet2());
        context.addServlet(empresaServlet2, "/secured/empresa2");

        ServletHolder listempresa = new ServletHolder(new ListEmpresa());
        context.addServlet(listempresa, "/secured/listempresa");

        
        ServletHolder usuarioServlet = new ServletHolder(new UsuarioServlet());
        context.addServlet(usuarioServlet, "/secured/usuario");

        
        ServletHolder buscarusuario = new ServletHolder(new BuscarUsuario());
        context.addServlet(buscarusuario, "/secured/buscarusuario");
        
        
        ServletHolder busquedalogin = new ServletHolder(new BusquedaLogin());
        context.addServlet(busquedalogin, "/secured/busquedalogin");
        
        
        ServletHolder busquedanombre = new ServletHolder(new BusquedaNombre());
        context.addServlet(busquedanombre, "/secured/busquedanombre");
        
        
         
        ServletHolder busquedaapellido = new ServletHolder(new BusquedaApellido());
        context.addServlet(busquedaapellido, "/secured/busquedaapellido");
        
        
        
        
        

        ServletHolder addusuario = new ServletHolder(new AddUsuario());
        context.addServlet(addusuario, "/secured/addusuario");

         ServletHolder listadousuario = new ServletHolder(new ListadoUsuario());
        context.addServlet(listadousuario, "/secured/listadousuario");

        
        
        
        
        
        
        ServletHolder adminCert  = new ServletHolder(new AdminCert());
        context.addServlet(adminCert, "/secured/adminCert");

        ServletHolder setFirmaPass  = new ServletHolder(new SetFirmaPass());
        context.addServlet(setFirmaPass, "/secured/setFirmaPass");
     
        
        ServletHolder uploadCert  = new ServletHolder(new UploadCert());
        
        uploadCert.getRegistration().setMultipartConfig(new MultipartConfigElement(
            "",       // Ubicación donde se almacenan archivos temporales
            10485760, // Tamaño máximo del archivo (10 MB)
            20971520, // Tamaño máximo de la solicitud (20 MB)
            5242880   // Umbral del archivo antes de escribir en disco (5 MB)
        ));
           
        context.addServlet(uploadCert, "/secured/uploadCert");
     
        ServletHolder adminCaf  = new ServletHolder(new AdminCaf());
        context.addServlet(adminCaf, "/secured/adminCAF");
     
        
        
        ServletHolder deleteCAF  = new ServletHolder(new DeleteCaf());
        context.addServlet(deleteCAF, "/secured/deleteCAF");
     
        ServletHolder cafok  = new ServletHolder(new CafOk());
        context.addServlet(cafok, "/secured/cafok");
     
        
        ServletHolder cafdeleted  = new ServletHolder(new CafOk());
        context.addServlet(cafdeleted, "/secured/cafdeleted");
     
        
        ServletHolder errorcaf  = new ServletHolder(new ErrorCaf());
        context.addServlet(errorcaf, "/secured/errorcaf");
     
        
        
        
      
        
         ServletHolder indexservlet2 = new ServletHolder(new IndexServlet());
        context.addServlet(indexservlet2, "/index");
     
        
             
        
        ServletHolder uploadCAF  = new ServletHolder(new UploadCaf());
        
        
        
        
         uploadCAF.getRegistration().setMultipartConfig(new MultipartConfigElement(
            "",       // Ubicación donde se almacenan archivos temporales
            10485760, // Tamaño máximo del archivo (10 MB)
            20971520, // Tamaño máximo de la solicitud (20 MB)
            5242880   // Umbral del archivo antes de escribir en disco (5 MB)
        ));
           
        context.addServlet(uploadCAF, "/secured/uploadCAF");
        
        
        ServletHolder setpass  = new ServletHolder(new SetPassServlet());
        context.addServlet(setpass, "/secured/setpass");
        
        
        ServletHolder sendDTE = new ServletHolder(new SendDteResource());
        context.addServlet(sendDTE, "/api/sendDTE");
       
        ServletHolder trackingboleta = new ServletHolder(new TrackingBoleta());
        context.addServlet(trackingboleta, "/api/trackingboleta");
       
        ServletHolder getDTE = new ServletHolder(new getDTE());
        context.addServlet(getDTE, "/api/getDTE");
       
        ServletHolder printdte = new ServletHolder(new printDTE());
        context.addServlet(printdte, "/api/printDTE");
       
        
        
        
        
        ServletHolder getTEDWS = new ServletHolder(new getTEDWS());
        context.addServlet(getTEDWS, "/api/getTEDWS");
       
        ServletHolder license = new ServletHolder(new License());
        context.addServlet(license, "/api/api-docs/license");
       
        
        ServletHolder apidocs = new ServletHolder(new APIDocs());
        context.addServlet(apidocs, "/api/api-docs");
       
        ServletHolder rootRedirectServlet = new ServletHolder(new RootRedirectServlet());
        context.addServlet(rootRedirectServlet, "/");
        
        
        
       
        
        // Configurar el servidor para manejar JSP y recursos REST
        server.setHandler(context);
        
         // Iniciar el servidor
        server.start();
        server.join();
     

    }
}
