/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/

package com.egga.appdtews.appboleta.xml;

import com.egga.appdtews.appdte.json.DescGlobalJson;
import com.egga.appdtews.appdte.json.DetalleDteJson;
import com.egga.appdtews.appdte.json.DteJson;
import com.egga.appdtews.appdte.json.EmisorJson;
import com.egga.appdtews.appdte.json.IdDteJson;
import com.egga.appdtews.appdte.json.ReceptorJson;
import com.egga.appdtews.appdte.sii.utilidades.ConfigAppDTE;
import com.egga.appdtews.appdte.sii.utilidades.SignDTE;
import com.egga.appdtews.appdte.sii.utilidades.Timbre;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class AppBoleta {
    
    String environment;
    
    
    public AppBoleta(String loginuser, String environment) throws FileNotFoundException, IOException{
        
     this.environment = environment;
     
     
     
    }
    

 @SuppressWarnings("empty-statement")
    
    public void generaBoleta(String stringDTE,String certificado,String clave,String rutEnvia, boolean blReferencia) throws TransformerException, ParserConfigurationException, SAXException, IOException, Exception{
      String login = certificado;
   ConfigAppDTE objconfig = new ConfigAppDTE();
        /* CARGO LOS PARAMETROS DE CONFIGURACION */
        /* ingreso el DTE en formato JSON */
  
             
   System.out.println("Reading JSON from a file");
   System.out.println("----------------------------");
  
   
  
    
  
     ObjectMapper objectMapper = new ObjectMapper();
	//convert json string to object
    DteJson objdtejson = objectMapper.readValue(stringDTE, DteJson.class);
       
    
    ReceptorJson objreceptor = objdtejson.getReceptor();
    EmisorJson objemisor = objdtejson.getEmisor();
    IdDteJson iddoc = objdtejson.getIddoc();
    
    String[] arrayrutemisor = objemisor.getRutemisor().split("-");
    
    String rutemisor = arrayrutemisor[0];
    String nombredte = "DTE"+rutemisor+"F"+iddoc.getFolio()+"T"+iddoc.getTipodte();
        
    
    
     
     /* DATOS DEL EMISOR EN JSON */
  
   /* inicializar el xml */        
   
    xmlBOLETA obj = new xmlBOLETA();
    
      /* cargo los detalles */
     List<DetalleDteJson> detalle = objdtejson.getDetalle();
  
    /*
   Timbre objTimbre = new Timbre(objconfig.getPathdte(),nombredte,pathdata,pathcaf);
   
   /*
   String pathdte,String nombredte, String pathdata,String pathcaf, String parmrut
   */
 
for (DetalleDteJson i :  detalle){     
 
   
    obj.agregaDetalle(i);
    
    }
    

/* ADJUNTO LOS DESCUENTOS GLOBALES   */

List<DescGlobalJson> arraydescuentos = objdtejson.getDscrcgglobal();

if(arraydescuentos!=null){ 
for (DescGlobalJson x :  arraydescuentos){
    
   obj.agregaDescuento(x);
   
   
}     
}  
  
obj.guardarDocumento(nombredte,objconfig.getPathdte());


/*


    
    public void creaTimbre( String pathdte,String nombredte, String pathdata,String pathcaf, String parmrut ) 

objTimbre.creaTimbre(objdte, auxDescripcion,rutemisor);
  
    
/* preparo el DTE para firmar */

Timbre objTimbre = new Timbre();
objTimbre.creaTimbre(login, objconfig.getPathdte(),nombredte,objconfig.getPathdata(),objconfig.getPathcaf(),rutemisor);
   

SignDTE objFirma = new SignDTE();
objFirma.signDTE(objconfig.getPathdte(),nombredte,certificado,clave);

  
/* ahora envuelvo el DTE en un sobre electrónico */

EnvioBOLETA objenvio = new EnvioBOLETA(this.environment, objreceptor.getRutcaratula());
objenvio.generaEnvio(objdtejson,nombredte,objconfig.getPathdte(),rutEnvia);


SignENVBOLETA objFirmaENV = new SignENVBOLETA();
objFirmaENV.signENVBOLETA(objconfig.getPathdte(),nombredte,certificado,clave);
  

  
 
}


    
}
