/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
package com.egga.appdtews.appboleta.sii;

import com.egga.appdtews.documento.DocumentoModel;
import com.egga.appdtews.empresa.EmpresaModel;
import com.egga.appdtews.movimientos.Movimiento;
import com.egga.appdtews.movimientos.MovimientoModel;
import com.egga.appdtews.appboleta.xml.AppBoleta;
import com.egga.appdtews.appdte.json.DteJson;
import com.egga.appdtews.appdte.json.EmisorJson;
import com.egga.appdtews.appdte.json.IdDteJson;
import com.egga.appdtews.appdte.json.ReceptorJson;
import com.egga.appdtews.appdte.json.TotalesJson;
import com.egga.appdtews.appdte.json.TrackID;
import com.egga.appdtews.appdte.sii.utilidades.ConfigAppDTE;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class sendBOLETA {
    
    
 public String sendBOLETA(String jsonDTE,  String loginuser,  String password,  String rutenvia) throws IOException, MalformedURLException, ParserConfigurationException, SAXException, XPathExpressionException, TransformerException, TransformerConfigurationException, Exception{
     
     /*
     System.setProperty("com.sun.org.apache.xml.internal.security.ignoreLineBreaks","true");
      */
     ConfigAppDTE objconfig = new ConfigAppDTE();
        
    seedBOLETA  objSemilla = new seedBOLETA();
    TokenBOLETA objToken = new TokenBOLETA(objconfig.getEnvironmentBoleta());
    String valorsemilla = objSemilla.getSeed(objconfig.getEnvironmentBoleta());
    String  stringToken = objToken.getToken(valorsemilla, loginuser, password);
    
    
    AppBoleta objBoleta = new AppBoleta("","");
    
    
    
    objBoleta.generaBoleta(jsonDTE,loginuser,password,rutenvia,true);
    
    
     InputStream isjson = new ByteArrayInputStream(jsonDTE.getBytes("UTF-8")); 
    BufferedReader br1 = new BufferedReader(new InputStreamReader(isjson));
  
  
   
   ObjectMapper objectMapperDTE = new ObjectMapper();
   DteJson objdtejson = objectMapperDTE.readValue(jsonDTE,DteJson.class); 
    
    
    
    
    
     
     /* DATOS DEL EMISOR EN JSON */
     EmisorJson objemisor = objdtejson.getEmisor();
     
     
     
     
     
    /* DATOS DEL RECEPTOR EN JSON */
    ReceptorJson objreceptor = objdtejson.getReceptor();
    IdDteJson iddoc = objdtejson.getIddoc();
    
    
    
    UpBOLETASII objupload = new UpBOLETASII(objconfig.getUploadBoleta());
    
    /* GUARDO LOS DATOS DE ENVIO EN LA BASE DE DATOS */
    
   String rutemisor = objemisor.getRutemisor();
   String[] arrayrutemisor = rutemisor.split("-");
   String rutusuario = rutenvia;
   String codsii = iddoc.getTipodte();
   String nombredte ="ENVDTE"+ arrayrutemisor[0]+"F"+iddoc.getFolio()+"T"+codsii;
   EmpresaModel objEmpresaModel = new EmpresaModel();  
   int empresaid = objEmpresaModel.getIdEmpresafromRut(objemisor.getRutemisor());
   DocumentoModel objDocumentoModel = new DocumentoModel();
   int idtipodoc = objDocumentoModel.getIddocumento2(Integer.parseInt(iddoc.getTipodte()));
   Movimiento objMovimiento = new Movimiento();
   TotalesJson objtotales = objdtejson.getTotales();
   objMovimiento.setFecha(iddoc.getFchemis());
   objMovimiento.setEmpresaid(empresaid);
   objMovimiento.setRutemisor(rutemisor);
   objMovimiento.setRutreceptor(objreceptor.getRutrecep());
   objMovimiento.setRutenvia(rutenvia);
   
   objMovimiento.setIdtipodoc(idtipodoc);
   objMovimiento.setTotalbruto(objtotales.getMnttotal());
   objMovimiento.setMontoexento(objtotales.getMntexe());
   objMovimiento.setMontoiva(objtotales.getIva());
   objMovimiento.setMontoneto(objtotales.getMntneto());
   objMovimiento.setMontonofacturado(objtotales.getMontonf());
   objMovimiento.setTipoOperacion(objdtejson.getTipoOperacion());
   objMovimiento.setMontoperiodo(objtotales.getMntperiodo());
   
   objMovimiento.setNumdoc(Integer.parseInt(iddoc.getFolio()));
   MovimientoModel objMovimientoModel = new MovimientoModel();
   
   
    String stringBOLETA =  objupload.upBOLETA(stringToken,nombredte,rutemisor,rutusuario);
    ObjectMapper objectMapperDTEBOLETA = new ObjectMapper();
    
    TrackID objTrack = new TrackID();
        StringWriter stringTRACK = new StringWriter();
        objectMapperDTEBOLETA.writeValue(stringTRACK, objTrack);	    
    
    
    
    
    
    objMovimiento.setTrackid(objTrack.getTrackid());
    objMovimiento.setArchivonom(objTrack.getFile());
    objMovimientoModel.addDTE(objMovimiento);
   
    return stringTRACK.toString();
     
    
  
    }
            
}
