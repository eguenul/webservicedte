/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
package com.egga.appdtews.appboleta.sii;

import com.egga.appdtews.appdte.json.stateJson;
import com.egga.appdtews.appdte.sii.utilidades.ConfigAppDTE;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class ConsultaBOLETA {
     
private String RutConsultante; 
private String DvConsultante; 
private String RutCompania;
private String DvCompania;         
private String RutReceptor;       
private String DvReceptor;
private String TipoDte;
private String FolioDte;
private String FechaEmisionDte;
private String MontoDte;       

public String getEstDte( String login, String clave) throws ParserConfigurationException, SAXException, IOException, Exception{
    seedBOLETA  objSemilla = new seedBOLETA();
    ConfigAppDTE objConfigAppDTE = new ConfigAppDTE();
    
    
    TokenBOLETA objToken = new TokenBOLETA(objConfigAppDTE.getEnvironmentBoleta());
    String valorsemilla = objSemilla.getSeed(objConfigAppDTE.getEnvironmentBoleta());
    String  stringToken = objToken.getToken(valorsemilla, login, clave);
    
    String estadoboleta = getEstadoEnvio(stringToken,objConfigAppDTE.getEnvironmentBoleta());
    
     InputStream isjson = new ByteArrayInputStream(estadoboleta.getBytes("UTF-8")); 
    BufferedReader br1 = new BufferedReader(new InputStreamReader(isjson));
  
    ObjectMapper objectMapperDTE = new ObjectMapper();
    stateJson objestadoboleta = objectMapperDTE.readValue(estadoboleta,stateJson.class); 
    return objestadoboleta.getCodigo();
    
    
    }
public String getEstadoEnvio(String valortoken, String environment) throws IOException{
    
    StringBuilder result = new StringBuilder();
    String urlconsulta = "https://"+environment+"/recursos/v1/boleta.electronica/"+getRutCompania()+"-"+getDvCompania()+"-"+getTipoDte()+"-"+getFolioDte()+"/estado?rut_receptor="+getRutReceptor()+"&dv_receptor="+ getDvReceptor()+"&monto="+ getMontoDte()+"&fechaEmision="+getFechaEmisionDte();
    System.out.print(urlconsulta);
    URL url = new URL(urlconsulta);
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.setRequestMethod("GET");
    conn.setRequestProperty("Cookie","TOKEN="+valortoken);
    /*
    conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=9022632e1130lc4");
    */
    try (BufferedReader reader = new BufferedReader(
            new InputStreamReader(conn.getInputStream()))) {
        for (String line; (line = reader.readLine()) != null; ) {
            result.append(line);
        }
        
        System.out.print(result.toString());
        return result.toString();
    }

}




    public String getRutConsultante() {
        return RutConsultante;
    }

    public void setRutConsultante(String RutConsultante) {
        this.RutConsultante = RutConsultante;
    }

    public String getDvConsultante() {
        return DvConsultante;
    }

    public void setDvConsultante(String DvConsultante) {
        this.DvConsultante = DvConsultante;
    }

    public String getRutCompania() {
        return RutCompania;
    }

    public void setRutCompania(String RutCompania) {
        this.RutCompania = RutCompania;
    }

    public String getDvCompania() {
        return DvCompania;
    }

    public void setDvCompania(String DvCompania) {
        this.DvCompania = DvCompania;
    }

    public String getRutReceptor() {
        return RutReceptor;
    }

    public void setRutReceptor(String RutReceptor) {
        this.RutReceptor = RutReceptor;
    }

    public String getDvReceptor() {
        return DvReceptor;
    }

    public void setDvReceptor(String DvReceptor) {
        this.DvReceptor = DvReceptor;
    }

    public String getTipoDte() {
        return TipoDte;
    }

    public void setTipoDte(String TipoDte) {
        this.TipoDte = TipoDte;
    }

    public String getFolioDte() {
        return FolioDte;
    }

    public void setFolioDte(String FolioDte) {
        this.FolioDte = FolioDte;
    }

    public String getFechaEmisionDte() {
        return FechaEmisionDte;
    }

    public void setFechaEmisionDte(String FechaEmisionDte) {
        this.FechaEmisionDte = FechaEmisionDte;
    }

    public String getMontoDte() {
        return MontoDte;
    }

    public void setMontoDte(String MontoDte) {
        this.MontoDte = MontoDte;
    }
           

}
