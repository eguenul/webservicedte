/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.usuarios;

import com.egga.appdtews.include.comonFunc;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class AddUsuario extends HttpServlet {
       
@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     
    Usuario objUsuario = new Usuario();
    UsuarioModel objUsuarioModel = new UsuarioModel();
    
    if(comonFunc.validaRut(request.getParameter("UsuarioRut"))==true){   
        try {
            objUsuario.setLogin(request.getParameter("UsuarioLogin"));
            objUsuario.setRut(request.getParameter("UsuarioRut"));
            objUsuario.setUsuarionom(request.getParameter("UsuarioNombre"));
            objUsuario.setUsuarioap(request.getParameter("UsuarioApellido"));
            objUsuario.setPassword(request.getParameter("UsuarioPass"));
            objUsuario.setEmail(request.getParameter("UsuarioEmail"));
            objUsuarioModel.addUsuario(objUsuario);
            return;
        
        } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(AddUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
                  
                 }else{
                        
                 return;
                 }
    
}
    
}
