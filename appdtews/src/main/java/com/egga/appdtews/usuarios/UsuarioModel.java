/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/

package com.egga.appdtews.usuarios;

import com.egga.appdtews.include.Conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class UsuarioModel {
  
   
    
public void addUsuario(Usuario objusuario) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
      Conexion objConexion = new Conexion();
      objConexion.Conectar();
         
      String login = objusuario.getLogin();
      String password = objusuario.getPassword();
      String email = objusuario.getEmail();
      String rut = objusuario.getRut();
      String nombre = objusuario.getUsuarionom();
      String apellido = objusuario.getUsuarioap();
      String sql = "insert into Usuario (UsuarioLogin,UsuarioPass,UsuarioRut,UsuarioEmail,UsuarioNom,UsuarioApell) \n"
                    +"values ("+"'"+login+"','"+password+"','"+rut+"',"+"'"+email+"',"+"'"+nombre+"','"+apellido+"')";
      
      Statement smt = objConexion.getConexion().createStatement();
      smt.execute(sql);    
      objConexion.cerrar();
   }
    
   
   public void updateUsuario(Usuario objusuario) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
        Conexion objConexion = new Conexion();
            objConexion.Conectar();
         
       
       
       String login = objusuario.getLogin();
      String password = objusuario.getPassword();
      String email = objusuario.getEmail();
      String rut = objusuario.getRut();
      String nombre = objusuario.getUsuarionom();
      String apellido = objusuario.getUsuarioap();
     
       String sql = "update Usuario set "+ 
                     "UsuarioPass='"+password+"', \n"+
                     "UsuarioRut='"+rut+"', \n"+
                     "UsuarioNom='"+nombre+"',\n"+
                     "UsuarioApell='"+apellido+"',\n"+
                     "UsuarioEmail ='"+email+"' \n"+
                     "where UsuarioLogin='"+login+"'\n";
   System.out.print(sql);
     
     
      
      
      Statement smt = objConexion.getConexion().createStatement();
      smt.execute(sql);     
      objConexion.cerrar();
      
   }
   
   
   public Usuario getUsuario(String usuariologin) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
        Conexion objConexion = new Conexion();
        objConexion.Conectar();
         
      
       String sql = "Select * from Usuario where UsuarioLogin='"+usuariologin +"'";
       System.out.print(sql);
       Usuario objusuario = new Usuario();
       Statement smt = objConexion.getConexion().createStatement();
       ResultSet objrecordset = smt.executeQuery(sql);  
       while(objrecordset.next()){
            objusuario.setEmail(objrecordset.getString("UsuarioEmail"));
            objusuario.setRut(objrecordset.getString("UsuarioRut"));
            objusuario.setUsuarioap(objrecordset.getString("UsuarioApell"));
            objusuario.setUsuarionom(objrecordset.getString("UsuarioNom"));
            objusuario.setLogin(objrecordset.getString("UsuarioLogin"));
            objusuario.setPassword(objrecordset.getString("UsuarioPass"));
        }
       
       objConexion.cerrar();
       return objusuario;
   }
   
   public ArrayList<Usuario> listUsuario() throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
        Conexion objConexion = new Conexion();
            objConexion.Conectar();
         
      
       ArrayList<Usuario> arraylista = new ArrayList<>();
       String sql = "Select * from Usuario where UsuarioLogin<>'admin' limit 0,10";
       Statement smt = objConexion.getConexion().createStatement();
       ResultSet objrecordset = smt.executeQuery(sql);  
       
       while(objrecordset.next()){
            Usuario objusuario = new Usuario();
            objusuario.setEmail(objrecordset.getString("UsuarioEmail"));
            objusuario.setRut(objrecordset.getString("UsuarioRut"));
            objusuario.setUsuarioap(objrecordset.getString("UsuarioApell"));
            objusuario.setUsuarionom(objrecordset.getString("UsuarioNom"));
            objusuario.setLogin(objrecordset.getString("UsuarioLogin"));
            objusuario.setPassword(objrecordset.getString("UsuarioPass"));
            arraylista.add(objusuario);
       }
       objConexion.cerrar();
       return arraylista;
   }
   
    public ArrayList<Usuario> searchLogin (String usuariologin) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
           Conexion objConexion = new Conexion();
            objConexion.Conectar();
         
        
       ArrayList<Usuario> arraylista = new ArrayList<>();
       
      String sql = "select * from Usuario where UsuarioLogin LIKE '"+usuariologin+"%' and UsuarioLogin<>'admin'";
      
       Statement smt = objConexion.getConexion().createStatement();
       ResultSet objrecordset = smt.executeQuery(sql);  
       
       while(objrecordset.next()){
            Usuario objusuario = new Usuario();
            objusuario.setEmail(objrecordset.getString("UsuarioEmail"));
            objusuario.setRut(objrecordset.getString("UsuarioRut"));
            objusuario.setUsuarioap(objrecordset.getString("UsuarioApell"));
            objusuario.setUsuarionom(objrecordset.getString("UsuarioNom"));
            objusuario.setLogin(objrecordset.getString("UsuarioLogin"));
            objusuario.setPassword(objrecordset.getString("UsuarioPass"));
            arraylista.add(objusuario);
       }
       objConexion.cerrar();
        return arraylista;
    }
    
    
     public ArrayList<Usuario> searchRut (String usuariorut) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
        Conexion objConexion = new Conexion();
            objConexion.Conectar();
         
        
       ArrayList<Usuario> arraylista = new ArrayList<>();
       
      String sql = "select * from Usuario where UsuarioRut LIKE '"+usuariorut+"%' and UsuarioLogin<>'admin'";
      
       Statement smt = objConexion.getConexion().createStatement();
       ResultSet objrecordset = smt.executeQuery(sql);  
       
       while(objrecordset.next()){
            Usuario objusuario = new Usuario();
            objusuario.setEmail(objrecordset.getString("UsuarioEmail"));
            objusuario.setRut(objrecordset.getString("UsuarioRut"));
            objusuario.setUsuarioap(objrecordset.getString("UsuarioApell"));
            objusuario.setUsuarionom(objrecordset.getString("UsuarioNom"));
            objusuario.setLogin(objrecordset.getString("UsuarioLogin"));
            objusuario.setPassword(objrecordset.getString("UsuarioPass"));
            arraylista.add(objusuario);
       }
       objConexion.cerrar();
        return arraylista;
    }
    
    
     public ArrayList<Usuario> searchNom (String usuarionom) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
          Conexion objConexion = new Conexion();
            objConexion.Conectar();
         
        
       ArrayList<Usuario> arraylista = new ArrayList<>();
       
      String sql = "select * from Usuario where UsuarioNom LIKE '"+usuarionom+"%' and UsuarioLogin<>'admin'";
      
       Statement smt = objConexion.getConexion().createStatement();
       ResultSet objrecordset = smt.executeQuery(sql);  
       
       while(objrecordset.next()){
            Usuario objusuario = new Usuario();
            objusuario.setEmail(objrecordset.getString("UsuarioEmail"));
            objusuario.setRut(objrecordset.getString("UsuarioRut"));
            objusuario.setUsuarioap(objrecordset.getString("UsuarioApell"));
            objusuario.setUsuarionom(objrecordset.getString("UsuarioNom"));
            objusuario.setLogin(objrecordset.getString("UsuarioLogin"));
            objusuario.setPassword(objrecordset.getString("UsuarioPass"));
            arraylista.add(objusuario);
       }
       objConexion.cerrar();
        return arraylista;
    }
    
    
    
     public ArrayList<Usuario> searchApellido (String usuarioapellido) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException{
           Conexion objConexion = new Conexion();
            objConexion.Conectar();
         
        
       ArrayList<Usuario> arraylista = new ArrayList<>();
       
      String sql = "select * from Usuario where UsuarioApell LIKE '"+usuarioapellido+"%' and  UsuarioLogin<>'admin'";
      
       Statement smt = objConexion.getConexion().createStatement();
       ResultSet objrecordset = smt.executeQuery(sql);  
       
       while(objrecordset.next()){
            Usuario objusuario = new Usuario();
            objusuario.setEmail(objrecordset.getString("UsuarioEmail"));
            objusuario.setRut(objrecordset.getString("UsuarioRut"));
            objusuario.setUsuarioap(objrecordset.getString("UsuarioApell"));
            objusuario.setUsuarionom(objrecordset.getString("UsuarioNom"));
            objusuario.setLogin(objrecordset.getString("UsuarioLogin"));
            objusuario.setPassword(objrecordset.getString("UsuarioPass"));
            arraylista.add(objusuario);
       }
       objConexion.cerrar();
        return arraylista;
    }
    
     
     
     
     
     
     
     
     
    
    
    
    
}
