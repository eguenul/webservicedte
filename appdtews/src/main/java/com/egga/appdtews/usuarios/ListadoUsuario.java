/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.usuarios;

import com.egga.appdtews.include.ThymeleafConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class ListadoUsuario extends HttpServlet {
    private TemplateEngine templateEngine;

   @Override
   public void init() {
        // Inicialización del TemplateEngine usando ThymeleafConfig
        ThymeleafConfig thymeleafConfig = new ThymeleafConfig();
        templateEngine = thymeleafConfig.createTemplateEngine();
   }
   
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            UsuarioModel objUsuarioModel = new UsuarioModel();
            ArrayList<Usuario> arraylistusuario = objUsuarioModel.listUsuario();
            Context context = new Context();
            context.setVariable("arraylistusuario",arraylistusuario );
            String contenido = templateEngine.process("/usuarioview/listadousuario", context);
            response.setContentType("text/html");
            response.getWriter().write(contenido);
            
            
            
        } catch (SQLException | ClassNotFoundException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(ListadoUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    
    
    }
   
   
   
   
}
