/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.usuarios;

import com.egga.appdtews.include.comonFunc;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */
public class UpdateUsuario extends HttpServlet {
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  try{
        Usuario objUsuario = new Usuario();
   UsuarioModel objUsuarioModel = new UsuarioModel();
                    
  if(comonFunc.validaRut(request.getParameter("UsuarioRut"))==true){   
    
           objUsuario.setLogin(request.getParameter("UsuarioLogin"));
           objUsuario.setRut(request.getParameter("UsuarioRut"));
           objUsuario.setUsuarionom(request.getParameter("UsuarioNombre"));
           objUsuario.setUsuarioap(request.getParameter("UsuarioApellido"));
           objUsuario.setPassword(request.getParameter("UsuarioPass"));
           objUsuario.setEmail(request.getParameter("UsuarioEmail"));
           objUsuarioModel.updateUsuario(objUsuario);
           
           objUsuario.setLogin("");
           objUsuario.setRut("");
           objUsuario.setUsuarionom("");
           objUsuario.setUsuarioap("");
           objUsuario.setEmail("");
           objUsuario.setPassword("");
           
           request.getSession().setAttribute("objUsuario", objUsuario);
           request.getSession().setAttribute("estado", "OK");
           request.getSession().setAttribute("ACC", "GRABAR");
       }else{
      
  }
  }catch (IOException | ClassNotFoundException | SQLException | ParserConfigurationException | SAXException ex){
          
          
  }
      
  }
    
                    
                    
        
        
        
    
    
}
