/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.egga.appdtews.usuarios;

import com.egga.appdtews.include.Funciones;
import com.egga.appdtews.appdte.sii.utilidades.ConfigAppDTE;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.sql.SQLException;
import java.util.Enumeration;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */

@WebServlet(urlPatterns = "/setFirmaPass")
public class SetFirmaPass extends HttpServlet{
@Override
public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                  getServletConfig().getServletContext().getRequestDispatcher("/usuarioview/setpassfirma.jsp").forward(request,response);
              
        
}

@Override
public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 try{      
     
     
    Funciones objFunciones = new Funciones();
   
      
    String login = (String) request.getSession().getAttribute("login");
    objFunciones.loadCert(login);
     
    
    String password = request.getParameter("ClaveFirma");
    ConfigAppDTE objConfigAppDTE = new ConfigAppDTE();     
    KeyStore p12 = KeyStore.getInstance("pkcs12");
    p12.load(new FileInputStream(objConfigAppDTE.getPathcert()+login+".pfx"), password.trim().toCharArray());
    Enumeration e = p12.aliases();
    String alias = (String) e.nextElement();
       
    System.out.println("Alias certifikata:" + alias);
    System.out.print("clave ok");
     
   
         objFunciones.setClaveFirma(login, password);
   
    response.sendRedirect("messageview/passfirmaok.html");
     
    }catch(IOException | ClassNotFoundException | KeyStoreException | NoSuchAlgorithmException | CertificateException | SQLException | ParserConfigurationException | SAXException ex){
     System.out.print("error de clave");
       
        response.sendRedirect("messageview/passfirmaerror.html");
    
          
    } 
      
}    
    
    
    
    
    
    
    
    
}
