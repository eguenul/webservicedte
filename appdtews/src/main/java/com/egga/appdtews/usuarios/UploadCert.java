/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
package com.egga.appdtews.usuarios;

import com.egga.appdtews.include.Funciones;
import com.egga.appdtews.appdte.sii.utilidades.ConfigAppDTE;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

@WebServlet(urlPatterns = "/uploadCert")
@MultipartConfig // Necesario para habilitar la carga de archivos
public class UploadCert extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String login = (String) request.getSession().getAttribute("login");
            
            if (login == null) {
                response.sendRedirect("/AppDTEWS/messageview/error.html");
                return;
            }
            
            ConfigAppDTE objConfig = new ConfigAppDTE();
            String pathCert = objConfig.getPathcert();
            String fileName = "";
            
            try {
                // Obtener la parte que contiene el archivo subido
                Part filePart = request.getPart("file"); // El campo "name" del input file debe ser "file"
                if (filePart != null) {
                    fileName = getFileName(filePart);
                    if (fileName == null || fileName.isEmpty()) {
                        System.out.print("error de ");
                        response.sendRedirect("/AppDTEWS/messageview/error.html");
                        return;
                    }

                    File uploadedFile = new File(pathCert + login + File.separator + fileName);
                    
                    // Crear directorio si no existe
                    File directory = new File(pathCert + login);
                    if (!directory.exists()) {
                        directory.mkdirs();
                    }

                    // Guardar el archivo en el sistema de archivos
                    filePart.write(uploadedFile.getAbsolutePath());
                    System.out.println("Archivo subido: " + fileName);
                    
                    // Procesar el archivo subido
                    try {
                  
                        Funciones objFunciones = new Funciones();
                        objFunciones.addCertificado(login, uploadedFile.getAbsolutePath());
                        System.out.print("Certificado guardado");
                        response.sendRedirect("/AppDTEWS/messageview/cafok.html");
                          return;
                    } catch (NullPointerException ex3) {
                        System.out.println("ERROR: Empresa no seleccionada.");
                        response.sendRedirect("/AppDTEWS/messageview/errorfile.html");
                          return;
                    }
                } else {
                    request.setAttribute("message", "No se ha subido ningún archivo.");
                    response.sendRedirect("/AppDTEWS/messageview/error.html");
                      return;
                }
            } catch (IllegalStateException | ParserConfigurationException | SAXException | SQLException | ClassNotFoundException ex) {
                Logger.getLogger(UploadCert.class.getName()).log(Level.SEVERE, null, ex);
                response.sendRedirect("/AppDTEWS/messageview/error.html");
                  return;
            }
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(UploadCert.class.getName()).log(Level.SEVERE, null, ex);
              return;
        }
    }

    // Método auxiliar para obtener el nombre del archivo subido
    private String getFileName(Part part) {
        String contentDisposition = part.getHeader("content-disposition");
        
        if (contentDisposition == null) {
            return null; // Evitar NullPointerException
        }

        // Dividir el header y buscar la parte con 'filename'
        for (String token : contentDisposition.split(";")) {
            if (token.trim().startsWith("filename")) {
                // Asegurarse de que el nombre del archivo se obtiene correctamente
                String fileName = token.substring(token.indexOf('=') + 1).trim().replace("\"", "");
                // Evitar posibles problemas con rutas en diferentes sistemas operativos
                return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1);
            }
        }
        return null;
    }
}
