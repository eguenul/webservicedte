/*
 * Copyright (C) [2024] [EGGA INFORMATICA E.I.R.L]
 *
 * Este programa es software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la Licencia Pública General de GNU publicada por
 * la Free Software Foundation, ya sea la versión 3 de la Licencia, o
 * (a tu elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía implícita de
 * COMERCIABILIDAD o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.
 * Consulta la Licencia Pública General de GNU para más detalles.
 *
 * Deberías haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa. Si no, visita <https://www.gnu.org/licenses/>.
*/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.egga.appdtews.usuarios;

/**
 *
 * @author esteban
 */
public class Usuario {
    private String login;
    private String password;
    private String rut;
    private String usuarionom;
    private String usuarioap;
    private String email;
    private String clavefirma;
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getUsuarionom() {
        return usuarionom;
    }

    public void setUsuarionom(String usuarionom) {
        this.usuarionom = usuarionom;
    }

    public String getUsuarioap() {
        return usuarioap;
    }

    public void setUsuarioap(String usuarioap) {
        this.usuarioap = usuarioap;
    }

    public String getClavefirma() {
        return clavefirma;
    }

    public void setClavefirma(String clavefirma) {
        this.clavefirma = clavefirma;
    }
    
    
}
