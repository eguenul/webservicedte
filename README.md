#AppDTEWS
Descripción
AppDTEWS es una aplicación que proporciona un servicio web REST para la carga de folios y la generación de facturas electrónicas. Además, incluye funcionalidades de administración mediante servlets y JSPs. La aplicación se ejecuta en un contenedor Jetty embebido y está diseñada para ser desplegada de manera eficiente usando Docker.

Instalación
Para instalar y ejecutar AppDTEWS, sigue estos pasos:

Clonar el Repositorio:

bash
Copiar código
git clone https://gitlab.com/eguenul/AppDTEWS.git
cd AppDTEWS
Construir el Proyecto: Asegúrate de tener Maven instalado y ejecuta:

bash
Copiar código
mvn clean package
Construir la Imagen Docker: Ejecuta el siguiente comando en el directorio raíz del proyecto:

bash
Copiar código
docker build -t appdtews .
Ejecutar el Contenedor: Inicia el contenedor con:

bash
Copiar código
docker run -d -p 8080:8080 appdtews
Uso
Una vez que el contenedor esté en ejecución, puedes acceder al servicio REST en http://localhost:8080/api. Para las páginas de administración, accede a http://localhost:8080/admin.

Configuración
La aplicación utiliza archivos de configuración para definir rutas y parámetros de operación. Asegúrate de que las siguientes carpetas están configuradas correctamente en el contenedor:

path-certificate
path-caf
path-pdf
path-data
path-DTE
path-img
path-template
path-download
path-privatekey
environment-url
environment-boleta
post-boleta
server-auth
server-acceptdte
Contribución
Si deseas contribuir al proyecto, por favor sigue estos pasos:

Fork el Repositorio.
Crea una Rama Nueva:
bash
Copiar código
git checkout -b nombre-de-tu-rama
Realiza tus Cambios y Haz Commit:
bash
Copiar código
git add .
git commit -m "Descripción de tus cambios"
Push a tu Repositorio:
bash
Copiar código
git push origin nombre-de-tu-rama
Crea un Pull Request en GitLab.
Soporte
Para obtener ayuda, abre un issue en GitLab o contacta a través del correo electrónico de soporte proporcionado en el repositorio.

Licencia
Este proyecto está licenciado bajo la Licencia MIT. Consulta el archivo LICENSE para obtener más detalles.

Estado del Proyecto
El desarrollo del proyecto está activo. Las actualizaciones y mejoras se implementan regularmente. Si tienes sugerencias o encuentras problemas, no dudes en informar a través de los issues.

